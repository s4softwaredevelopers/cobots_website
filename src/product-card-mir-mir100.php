
							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/robots/mir100.png" data-srcset="assets/img/products/robots/mir100.png" alt="MiR100" align="middle" style="padding: 60px 0">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>MiR100 | MiR</h2>

									

									<p class="card-text" style="text-align:justify">A new generation of autonomous mobile robots is changing the way businesses move materials inside their facilities—and the MiR100™ is leading the charge. With extraordinary flexibility and smart technology, the MiR100™ can be used in nearly any situation where employees are spending time pushing carts or making deliveries. Now you can automate these tasks, so employees can focus on higher value activities.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".mir-mir100-modal">More Information</button>

										<div class="modal fade mir-mir100-modal" tabindex="-1" role="dialog" aria-labelledby="mir-mir100-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">MiR100</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>The highly flexible MiR100™ autonomously transports up to 100 kg (220 lbs). It can be mounted with customized top modules such as bins, racks, lifts, conveyors or even a collaborative robot arm—whatever your application demands. Top modules are easy to change so the robot can be redeployed for different tasks.</p>

															<p>The MiR100™ robot safely maneuvers around people and obstacles, through doorways and in and out of elevators. You can download CAD files of the building directly to the robot, or program it with the simple, web-based interface that requires no prior programming experience. The robot’s mission can be easily adapted using a smartphone, tablet or computer connected to the network.</p>

															<p>With built-in sensors and cameras and sophisticated software, the MiR100™ can identify its surroundings and take the most efficient route to its destination, safely avoiding obstacles and people. Without the need to alter your facility with expensive, inflexible wires or sensors, the robot offers a fast return on investment, with payback in as little as a year.</p>

															<h3>Specs</h3>

															<table width="100%" style="text-align:left;">

																<tbody>

																	<tr>

																		<td width="50%"><strong>Length</strong></td>

																		<td width="50%">890mm</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Width</strong></td>

																		<td width="50%">580mm</td>

																	</tr>	

																	<tr>

																		<td width="50%"><strong>Height</strong></td>

																		<td width="50%">352mm</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Weight (without load)</strong></td>

																		<td width="50%">67kg</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Load Surface</strong></td>

																		<td width="50%">600mmx800mm</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Robot Payload</strong></td>

																		<td width="50%">100kg (maximum 5% incline)</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>IP Class</strong></td>

																		<td width="50%">IP 20</td>

																	</tr>

																</tbody>

															</table>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>