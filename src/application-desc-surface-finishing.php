<article class="portfolio-item">

							<div class="portfolio-image">

									<img data-toggle="modal" data-target=".surface-finishing-modal" src="assets/img/icons/applications/surface-finishing.png" alt="Surface Finishing">

							</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".surface-finishing-modal">Surface Finishing</a></h3>

										<div class="modal fade surface-finishing-modal" tabindex="-1" role="dialog" aria-labelledby="surface-finishing-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Surface Finishing</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>Ensure consistent polishing, buffing and deburring using a Universal Robot with built-in force mode - even on curved and uneven surfaces.</p>
														</div>

													</div>

												</div>

											</div>

										</div>

							</div>

						</article>