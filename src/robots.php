<!DOCTYPE html>

<html dir="ltr" lang="en-US" prefix="og: http://ogp.me/ns#">

<head>



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="Cobots (Pty) Ltd." />

	<meta name="description" content="View our selection of collaborative robots from top brands such as Universal Robots and MiR.">

	<meta name="robots" content="index, follow">

	
	<?php include ("favicon.php");?>


	<?php include ("stylesheets.php");?>

	<meta name="viewport" content="width=device-width, initial-scale=1" />

	


	<title>Introducing our selection of robots | Cobots (Pty) Ltd.</title>

	<!--OPENGRAPH-->

	<meta property="og:title" content="Our Collaborative Robots | Cobots (Pty) Ltd." />

	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://www.cobots.co.za/robots" />

	<meta property="og:image" content="https://www.cobots.co.za/assets/img/home/cobots-featured-image.png" />

		<meta property="og:image:type" content="image/png" />

		<meta property="og:image:width" content="1200" />

		<meta property="og:image:height" content="630" />

		<meta property="og:image:alt" content="Welcome to Cobots (Pty) Ltd. | Robots" />

</head>



<body class="stretched">


	<div id="wrapper" class="clearfix">



		<?php require("header.php"); ?>

		<script type="text/javascript" src="assets/js/lazyload.js"></script>


		<section id="page-title" class="page-title-dark page-title-center" style="background-image: url('https://www.cobots.co.za/assets/img/misc/robots-title-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">



			<div class="container clearfix">

				<h1>Our Robots</h1>

			</div>



		</section>




		<section id="content">

			<div class="content-wrap" style="background-color:#eeeeee">

				<div class="container clearfix">

							<?php include("product-card-universal-robot-ur3.php");?>

							

							

							<?php include("product-card-universal-robot-ur5.php");?>

							

							

							<?php include("product-card-universal-robot-ur10.php");?>

							

							

							<?php include("product-card-universal-robot-ur3e.php");?>

							

							

							<?php include("product-card-universal-robot-ur5e.php");?>

							

							

							<?php include("product-card-universal-robot-ur10e.php");?>

							

							<?php include("product-card-mir-mir500.php");?>

							

							

							<?php include("product-card-mir-mir100.php");?>

							

							

							<?php include("product-card-mir-mir200.php");?>

							

							

							<?php include("product-card-mir-mirhook100.php");?>

							

							

							<?php include("product-card-mir-mirhook200.php");?>

							

							<?php include("product-card-qbrobotics-qbmove.php");?>

							

							

							<?php include("product-card-qbrobotics-qbmove-advanced.php");?>

				</div>

			</div>

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/misc/robots-call-to-action-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<h2 style="color: #fff; text-align:center">Contact us to start your collaborative automation journey today!</h2>

						<div class="widget clearfix" style="text-align:center">

							<a href="https://www.cobots.co.za/contact" class="btn btn-contact">Contact Us</a>

						</div>

				</div>

			</div>

		</section>


		<?php require("footer.php"); ?>

</body>

</html>