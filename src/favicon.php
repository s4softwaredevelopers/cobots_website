<link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-touch-icon.png">

<link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png">

<link rel="icon" type="image/png" sizes="192x192" href="/assets/favicon/android-chrome-192x192.png">

<link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png">

<link rel="manifest" href="/assets/favicon/site.webmanifest">

<link rel="mask-icon" href="/assets/favicon/safari-pinned-tab.svg" color="#00aa00">

<meta name="msapplication-TileColor" content="#00aa00">

<meta name="msapplication-TileImage" content="/assets/favicon/mstile-144x144.png">

<meta name="theme-color" content="#00aa00">