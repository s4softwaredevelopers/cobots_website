

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/end-effectors/robotiq-adaptive-grippers.png" data-srcset="assets/img/products/end-effectors/robotiq-adaptive-grippers.png" alt="Robotiq Adaptive Grippers" align="middle" style="padding:15px 0">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>Adaptive Grippers | Robotiq</h2>

									<p class="card-text" style="text-align:justify">Robotiq's Adaptive Grippers are made specifically for collaborative robots. These Plug 'n Play adapters are easy to program and offer high payload and wide stroke options.</p>

									<p class="card-text" style="text-align:justify">The primary applications for these adapters include quality testing, machine tending, pick & place and assembly.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".robotiq-adaptive-grippers-modal">More Information</button>

										<div class="modal fade robotiq-adaptive-grippers-modal" tabindex="-1" role="dialog" aria-labelledby="robotiq-adaptive-grippers-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Adaptive Grippers</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p><strong>Easy to integrate and easy to use, features include:</strong></p>
																<div class="modal-list">
																<ul>

																	<li>3 wide-stroke gripper models for use with collaborative robots</li>

																	<li>Multiple grip modes for working with any application</li>

																	<li>Simple to program<li>

																	<li>Automatic part detection, position feedback, & part validation</li>

																	<li>Suit industrial needs</li>

																	<li>High force and payload</li>

																	<li>Precise and durable</li>

																</ul>
																</div>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>