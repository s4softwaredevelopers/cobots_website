

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/end-effectors/robotiq-3-finger-adaptive-gripper.png" data-srcset="assets/img/products/end-effectors/robotiq-3-finger-adaptive-gripper.png" alt="Robotiq 3 Finger Adaptive Robot Gripper" align="middle" style="margin-bottom: 25px; margin-top: 25px;">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>3-Finger Adaptive Robot Gripper | Robotiq</h2>

									<p class="card-text" style="text-align:justify">The 3-Finger Gripper is the best option for maximum versatility and flexibility. It picks up any object of any shape. It is ideal for advanced manufacturing and robotic research. It adapts to the object’s shape for a solid grip, so you can focus on the task and not the grasping.</p>

									<p class="card-text" style="text-align:justify">The primary applications for these adapters include quality testing, machine tending, pick & place and assembly.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".robotiq-3-finger-adaptive-gripper-modal">More Information</button>

										<div class="modal fade robotiq-3-finger-adaptive-gripper-modal" tabindex="-1" role="dialog" aria-labelledby="robotiq-3-finger-adaptive-gripper-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">3-Finger Adaptive Robot Gripper</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body modal-list">

															<p><strong>Easy to integrate and easy to use, features include:</strong></p>

																<ul>

																	<li>4 grip types (Pinch Mode, Wide Mode, Scissor Mode, Basic Mode)</li>

																	<li>Control fingers separately</li>

																	<li>Get feedback from each finger</li>

																	<li>Plug + Play</li>

																	<li>Install and program your robot in a few minutes with our Gripper URCap</li>

																</ul>

															<p><strong>Specs</strong></p>

															<table width="100%" style="text-align:left;">

																<tbody>

																	<tr>

																		<td width="50%"><strong>Gripper Opening</strong></td>

																		<td width="50%">0 to 155mm (0 to 6.1 in)</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Gripper Weight</strong></td>

																		<td width="50%">2.3kg (5 lbs)</td>

																	</tr>	

																	<tr>

																		<td width="50%"><strong>Object diameter for encompassing</strong></td>

																		<td width="50%">20 to 155mm (0.79 to 6.1 in)</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Maximum recommended payload (encompassing grip)</strong></td>

																		<td width="50%">10kg (22 lbs)</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Maximum recommended payload (fingertip grip)</strong></td>

																		<td width="50%">2.5kg (5.5 lbs)</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Grip force (fingertip grip)</strong></td>

																		<td width="50%">30 to 70N (6.74 to 15.74 lbf)</td>

																	</tr>

																</tbody>

															</table>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>
