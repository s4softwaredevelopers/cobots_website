<article class="portfolio-item">

							<div class="portfolio-image">

									<img data-toggle="modal" data-target=".vision-inspection-modal" src="assets/img/icons/applications/vision-inspection.png" alt="Vision Inspection">

							</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".vision-inspection-modal">Vision Inspection</a></h3>

								<div style="text-align:center">



										<div class="modal fade vision-inspection-modal" tabindex="-1" role="dialog" aria-labelledby="vision-inspection-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Vision Inspection</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>Vision Inspection can be implemented with a collaborative robot and an accessory such as the Robotiq Wrist Camera. The Robotiq's Wrist Camera's capabilities are part detection and localization and color validation.</p> 

															<p>The Universal Robot is essential in reducing inspection cycle times and provides consistency in 3D scanning from one operator to the next.  Universal Robots of all sizes have been integrated with a variety of 3D and 2D sensors to capture dense, high quality for data analysis.</p>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>

							</div>

						</article>