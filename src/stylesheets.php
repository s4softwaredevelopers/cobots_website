	<link href="https://fonts.googleapis.com/css?family=Teko" rel="stylesheet" type="text/css">

	<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css" />

	<link rel="stylesheet" href="assets/css/style.css" type="text/css" />

	<link rel="stylesheet" href="assets/css/swiper.css" type="text/css" />

	<link rel="stylesheet" href="assets/css/dark.css" type="text/css" />

	<link rel="stylesheet" href="assets/css/font-icons.css" type="text/css" />

	<link rel="stylesheet" href="assets/css/animate.css" type="text/css" />

	<link rel="stylesheet" href="assets/css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="assets/css/custom.css" type="text/css" />	

	<link rel="stylesheet" href="assets/css/responsive.css" type="text/css" />

	


	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
	<script>
	window.addEventListener("load", function(){
	window.cookieconsent.initialise({
  	"palette": {
   	 "popup": {
     	 "background": "#000000",
	     	 "text": "#ffffff"
	    },
	    "button": {
 	     "background": "#00aa00",
  	    "text": "#ffffff"
  	  }
 	 },
	  "position": "top",
 	 "static": true,
	  "content": {
	    "href": "/privacy-policy"
	  }
	})});
	</script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-76516479-2"></script>
	<script>
 	 window.dataLayer = window.dataLayer || [];
  	function gtag(){dataLayer.push(arguments);}
 	 gtag('js', new Date());

 	 gtag('config', 'UA-76516479-2');
	</script>
