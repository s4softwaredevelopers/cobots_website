
							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="https://www.cobots.co.za/assets/img/products/accessories/easyrobotics-er5-mobile-cobot-platform.png" data-srcset="https://www.cobots.co.za/assets/img/products/accessories/easyrobotics-er5-mobile-cobot-platform.png" alt="ER5 Mobile Cobot Platform" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>ER5 Mobile Cobot Platform | EasyRobotics</h2>

									<p class="card-text" style="text-align:justify">ER5 Mobile Cobot Platform is a compact mobile workstation for safe collaboration with robot arms.</p>

									<p class="card-text" style="text-align:justify">Secure robot arms and maximal mobility is a key part of the ER5. The ER5 is equipped with integrated rolls and handholds, facilitating easier movement and deployment. It is fully automated upon delivery with trays, a robot arm and control panel.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".easyrobotics-er5-cobot-platform-modal">More Information</button>

										<div class="modal fade easyrobotics-er5-cobot-platform-modal" tabindex="-1" role="dialog" aria-labelledby="easyrobotics-er5-cobot-platform-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">ER5 Mobile Cobot Platform</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body modal-list">
														<ul>	
															<li>ER5 was specifically designed for the UR3 and UR5 by Universal Robots</li>

															<li>Developed to maximize the mobility and flexibility of robots and cobots</li>

															<li>Facilitates the start up to automation within your production</li>

															<li>Enhances flexibility and effectiveness in production</li>
															<li>Can be moved without the assistance of an individual, forklift or a pallet truck</li>														

														</ul>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>