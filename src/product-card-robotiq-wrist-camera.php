

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/accessories/robotiq-wrist-camera.png" data-srcset="assets/img/products/accessories/robotiq-wrist-camera.png" alt="Robotiq Wrist Camera" align="middle" style="margin-top: 20px; margin-bottom: 20px;">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>Wrist Camera | Robotiq</h2>

									<p class="card-text" style="text-align:justify">The Wrist Camera is a Plug 'n Play adapter for Universal Robots. It enables part-detection, localisation and part teaching on UR teach pendant. It allows for color validation and is integrated on the robot's wrist.</p>

									<p class="card-text" style="text-align:justify">The Robotiq Wrist Camera is made for collaborative robot applications in highly flexible industrial environments. It allows quick part teaching and design changes for a fast time to production.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".robotiq-wrist-camera-modal">More Information</button>

										<div class="modal fade robotiq-wrist-camera-modal" tabindex="-1" role="dialog" aria-labelledby="robotiq-wrist-camera-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Wrist Camera</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body modal-list">

															<p><strong>Easy to integrate and easy to use, features include:</strong></p>

																<ul>

																	<li>Fits on all UR wrists and connects directly to the robot controller</li>

																	<li>Two customizable object teaching methods</li>

																	<li>Integrated lighting & focus adjustment (automatic & manual)</li>

																	<li>Color validation</li>

																	<li>No need of external PC or hardware to teach, edit or run</li>

																	<li>Compact & slim aluminum body, sealed hardware for industrial needs</li>

																</ul>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>
