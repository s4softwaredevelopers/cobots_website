<article class="portfolio-item">

							<div class="portfolio-image">

									<img data-toggle="modal" data-target=".infinite-possibilities-modal"  src="assets/img/icons/applications/infinite-possibilities.png" alt="Infinite Possibilities">

							</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".infinite-possibilities-modal">Infinite Possibilities</a></h3>

								<div style="text-align:center">



										<div class="modal fade infinite-possibilities-modal" tabindex="-1" role="dialog" aria-labelledby="infinite-possibilities-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Infinite Possibilities</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>The possibilities are endless when it comes to what applications you can achieve with collaborative robots. You are only limited by your imagination.</p>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>

							</div>

						</article>