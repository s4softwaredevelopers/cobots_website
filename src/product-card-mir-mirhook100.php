

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/robots/mirhook100.png" data-srcset="assets/img/products/robots/mirhook100.png" alt="MiRHook100" align="middle" style="padding:30px 0">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>MiRHook100 | MiR</h2>

									
									<p class="card-text" style="text-align:justify">MiRHook™100 is ideal for a wide range of towing jobs, such as efficiently moving heavy products between locations in a manufacturing facility or warehouse, or moving linen and food carts in hospitals. The MiRHook100™  supports the transport of loads up to 300 kg (661 lbs), providing exciting new internal logistics options for weighty or unwieldy cargos.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".mir-mirhook100-modal">More Information</button>

										<div class="modal fade mir-mirhook100-modal" tabindex="-1" role="dialog" aria-labelledby="mir-mirhook100-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">MiRHook100™</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>The MiRHook100™  robot identifies carts by QR markers and autonomously transports them as you define. MiRHook100™  can be incorporated into a fleet of MiR robots, and can be easily redeployed to meet changing requirements. Updating the robot's mission at any time is simple, using a smartphone, tablet or computer and standard Wi-Fi or Bluetooth communications to access the robot's intuitive controls.</p>

															<p>With the MiRHook100™ , you simply measure your cart and feed the data into the software. Built-in sensors, cameras and sophisticated software mean the robot with the MiRHook100™  and a cart can safely maneuver around people and obstacles, and can even drive up ramps.</p>

															<h3>Specs</h3>

															<table width="100%" style="text-align:left;">

																<tbody>

																	<tr>

																		<td width="50%"><strong>Length</strong></td>

																		<td width="50%">1180mm-1275 mm (highest to lowest positions of hook arm)</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Width</strong></td>

																		<td width="50%">580mm</td>

																	</tr>	

																	<tr>

																		<td width="50%"><strong>Height</strong></td>

																		<td width="50%">550mm to 900 mm (lowest to highest positions of hook arm)</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Height above floor</strong></td>

																		<td width="50%">Robot: 50 mm; Gripping height: 50 mm - 390 mm</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Weight (without load)</strong></td>

																		<td width="50%">98kg</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Towing Capacity</strong></td>

																		<td width="50%">Up to 300kg (1% incline)/ 200kg (5% incline)</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>IP Class</strong></td>

																		<td width="50%">IP20</td>

																	</tr>

																</tbody>

															</table>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>
