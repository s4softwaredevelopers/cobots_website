<article class="portfolio-item">

							<div class="portfolio-image">

									<img data-toggle="modal" data-target=".inventory-organisation-modal" src="assets/img/icons/applications/inventory-organisation+management.png" alt="Inventory Organisation and Management">

							</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".inventory-organisation-modal">Inventory Organisation and Management</a></h3>

										<div class="modal fade inventory-organisation-modal" tabindex="-1" role="dialog" aria-labelledby="inventory-organisation-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Inventory Organisation and Management</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>Automate inventory management by using collaborative robots. Upgrading your current warehouse infrastructure to include Mobile Industrial Robots will free up your workers for more value-added tasks. Automate everything from sorting and packing to the transport and organisation of items within your warehouse.</p>
														</div>

													</div>

												</div>

											</div>

										</div>

							</div>

						</article>