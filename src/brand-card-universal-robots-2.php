
<article class="portfolio-item">
	<div class="portfolio-image">
			<img src="https://www.cobots.co.za/assets/img/brands/universal-robots/universal-robots-brand-card.jpg" alt="Universal Robots">

		<div class="portfolio-overlay"></div>
	</div>
	<div class="portfolio-desc" style="background-color:#fff">
		<h3><a href="https://www.cobots.co.za/universal-robots">Universal Robots</a></h3>
		<p>Robots</p>

			<a class="btn btn-primary" href="https://www.cobots.co.za/universal-robots">View Page</a>

	</div>
</article>