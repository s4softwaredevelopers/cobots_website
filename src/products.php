<!DOCTYPE html>

<html dir="ltr" lang="en-US" prefix="og: http://ogp.me/ns#">

<head>



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="Cobots (Pty) Ltd." />

	<meta name="description" content="At Cobots we offer a wide variety of easy-to-use collaborative robot solutions. Take a look at all of our current products.">

	<meta name="robots" content="index, follow">

	<?php include ("favicon.php");?>


	<?php include ("stylesheets.php");?>

	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>Our Favourite Products | Cobots (Pty) Ltd.</title>

	<!--OPENGRAPH-->

	<meta property="og:title" content="All Products | Cobots (Pty) Ltd." />

	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://www.cobots.co.za/products" />

	<meta property="og:image" content="https://www.cobots.co.za/assets/img/home/cobots-featured-image.png" />

		<meta property="og:image:type" content="image/png" />

		<meta property="og:image:width" content="1200" />

		<meta property="og:image:height" content="630" />

		<meta property="og:image:alt" content="Welcome to Cobots (Pty) Ltd. | All Products" />

</head>



<body class="stretched">


	<div id="wrapper" class="clearfix">



		<?php require("header.php"); ?>
		
		<script type="text/javascript" src="assets/js/lazyload.js"></script>

		<section id="page-title" class="page-title-dark page-title-center" style="background-image: url('https://www.cobots.co.za/assets/img/misc/products-title-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">



			<div class="container clearfix">

				<h1>All of Our Products</h1>

			</div>



		</section>



		
		<section id="content">

			<div class="content-wrap" style="background-color:#eeeeee">

				<div class="container clearfix">


														
							<div id="portfolio">	

							<?php include("product-card-universal-robot-ur3.php");?>

							

							
							<?php include("product-card-universal-robot-ur5.php");?>
							

							

							<?php include("product-card-universal-robot-ur10.php");?>

							

							

							<?php include("product-card-universal-robot-ur3e.php");?>

							

							

							<?php include("product-card-universal-robot-ur5e.php");?>

							

							

							<?php include("product-card-universal-robot-ur10e.php");?>

							

							

							<?php include("product-card-robotiq-adaptive-grippers.php");?>

							

							

							<?php include("product-card-robotiq-3-finger-adaptive-gripper.php");?>

							

							

							<?php include("product-card-robotiq-wrist-camera.php");?>

							

							

							<?php include("product-card-robotiq-force-sensor.php");?>

							

							

							<?php include("product-card-robotiq-insights.php");?>

							

							

							<?php include("product-card-robotiq-skills.php");?>

							

							

							<?php include("product-card-it-robotics-eyetpick.php");?>

							

							

							<?php include("product-card-it-robotics-eyetinspect.php");?>

							

							

							<?php include("product-card-it-robotics-workcellsimulator.php");?>

							

							

							<?php include("product-card-mir-mir500.php");?>

							

							

							<?php include("product-card-mir-mir100.php");?>

							

							

							<?php include("product-card-mir-mir200.php");?>

							

							

							<?php include("product-card-mir-mirhook100.php");?>

							

							

							<?php include("product-card-mir-mirhook200.php");?>

							

							

							<?php include("product-card-mir-pallet-lift.php");?>

							

							

							<?php include("product-card-mir-pallet-rack.php");?>

							

							

							<?php include("product-card-mir-fleet.php");?>

							

							

							<?php include("product-card-mir-charge.php");?>

							

							

							<?php include("product-card-onrobot-rg2-gripper.php");?>

							

							

							<?php include("product-card-onrobot-rg6-gripper.php");?>

							

							

							<?php include("product-card-onrobot-dual-gripper.php");?>

							

							

							<?php include("product-card-onrobot-rg2-ft.php");?>

							

							

							<?php include("product-card-onrobot-gecko-gripper.php");?>

							

							

							<?php include("product-card-onrobot-polyskin-tactile-gripper.php");?>

							

							

							<?php include("product-card-onrobot-hex-torque-sensor.php");?>

							

							

							<?php include("product-card-onrobot-omd.php");?>

							

							

							<?php include("product-card-qbrobotics-qbmove.php");?>

							

							

							<?php include("product-card-qbrobotics-qbmove-advanced.php");?>

							

							

							<?php include("product-card-qbrobotics-qbsofthand-research.php");?>

							

							

							<?php include("product-card-qbrobotics-flat-flange-addon.php");?>

							<?php include("product-card-qbrobotics-flat-flange.php");?>

							<?php include("product-card-qbrobotics-c-flange.php");?>

							<?php include("product-card-robo-gear-industrial-robot-cover.php");?>

							<?php include("product-card-robo-gear-disposable-cover.php");?>

							<?php include("product-card-robo-gear-accordion-lift-cover.php");?>							

							<?php include("product-card-robo-gear-reiku-cabling-solutions.php");?>

							<?php include("product-card-robodk-software-pack.php");?>
							
							<?php include("product-card-easyrobotics-er5-mobile-cobot-platform.php");?>

							<?php include("product-card-easyrobotics-profeeder-light.php");?>

							<?php include("product-card-easyrobotics-profeeder.php");?>

							<?php include("product-card-easyrobotics-profeeder-multi.php");?>

							<?php include("product-card-easyrobotics-easy-plus.php");?>

							</div>

				</div>

			</div>

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/misc/products-call-to-action-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<h2 style="color: #fff; text-align:center">Contact us to start your collaborative automation journey today!</h2>

						<div class="widget clearfix" style="text-align:center">

							<a href="https://www.cobots.co.za/contact" class="btn btn-contact">Contact Us</a>

						</div>

				</div>

			</div>

		</section>


		<?php require("footer.php"); ?>

</body>

</html>