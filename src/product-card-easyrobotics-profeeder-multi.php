
							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="https://www.cobots.co.za/assets/img/products/accessories/easyrobotics-profeeder-multi.png" data-srcset="https://www.cobots.co.za/assets/img/products/accessories/easyrobotics-profeeder-multi.png" alt="Profeeder Multi" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>Profeeder Multi | EasyRobotics</h2>

									<p class="card-text" style="text-align:justify">ProFeeder Multi forms part of Stage 3 automation. ProFeeder Multi is the stationary solution applied to fully automated 4-shift operations and in the production of a large series.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".easyrobotics-profeeder-multi-modal">More Information</button>

										<div class="modal fade easyrobotics-profeeder-multi-modal" tabindex="-1" role="dialog" aria-labelledby="easyrobotics-profeeder-multi-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Profeeder Multi</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body modal-list">
														<ul>	
															<li>ROI: 1-5 years</li>

															<li>Operation of parts tray: Fully automated</li>

															<li>Mobility: Stationary</li>

															<li>Number of part trays: 2 x 4</li>
															
															<li>Movers: 2</li>		

															<li>Production: Fully automated 4-shift operation</li>	
															
															<li>Dimensions: 1780x860x2170mm</li>	
														</ul>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>