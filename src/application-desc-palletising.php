<article class="portfolio-item">

							<div class="portfolio-image">

									<img data-toggle="modal" data-target=".palletising-modal"  src="assets/img/icons/applications/palletising.png" alt="Packaging and Palletising">

							</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".palletising-modal">Packaging and Palletising</a></h3>

								<div style="text-align:center">



										<div class="modal fade palletising-modal" tabindex="-1" role="dialog" aria-labelledby="palletising-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Packaging and Palletising</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>Packing and palletizing can be easily automated. By atomizing your packing line, it will ensure that your deliveries are always correctly counted and packed to the strictest standards.</p>

															<p>A collaborative robot relieves workers from repetitive and heavy lifting. The collaborative solution operates alongside employees.</p>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>

							</div>

						</article>