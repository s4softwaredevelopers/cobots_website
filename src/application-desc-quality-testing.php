<article class="portfolio-item">

							<div class="portfolio-image">

									<img data-toggle="modal" data-target=".quality-testing-modal" src="assets/img/icons/applications/quality-testing.png" alt="Quality Testing">

							</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".quality-testing-modal">Quality Testing</a></h3>

								<div style="text-align:center">



										<div class="modal fade quality-testing-modal" tabindex="-1" role="dialog" aria-labelledby="quality-testing-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Quality Testing</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>Quality Testing and inspection  with a collaborative robot can increase precision and consistency of your product. A collaborative robot will consistently and repeatedly follow exact processes and pre-defined workflows with minuscule/little deviation which provides optimum conditions to conduct analysis or study.</p> 

															<p>Attaching an OnRobot end-of-arm tool on  the collaborative robot such as the OnRobot RG2 Gripper or RG6 Gripper ensures precision and consistency in your operation. With the programmable force of the OnRobot 2 Finger Gripper it can easily handle delicate items.</p>

															<p>A collaborative robot with the accessory of a vision camera can also be used for non-destructive testing and 3D measurements. A vision camera paired with a collaborative robot can objectively identify and pinpoint defective or faulty parts before they are packed or shipped.</p>

															<p>By having a collaborative robot a part of your company's quality testing's and inspection, your employees can be relieved from repetitive tasks and increase the consistency in your quality inspection or processes. </p>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>

							</div>

						</article>