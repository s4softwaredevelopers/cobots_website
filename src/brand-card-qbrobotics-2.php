<article class="portfolio-item">

									<div class="portfolio-image">


											<img src="https://www.cobots.co.za/assets/img/brands/qbrobotics/qbrobotics-brand-card.jpg" alt="qbRobotics">


										<div class="portfolio-overlay">

										</div>

									</div>

									<div class="portfolio-desc" style="background-color:#fff">

										<h3><a href="https://www.cobots.co.za/qbrobotics">qbRobotics</a></h3>

										<p>Robots | End-Effectors | Accessories</p>

												<a class="btn btn-primary" href="https://www.cobots.co.za/qbrobotics">View Page</a>


									</div>

								</article>