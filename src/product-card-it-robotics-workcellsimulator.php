

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/accessories/it-robotics-workcellsimulator.jpg" data-srcset="assets/img/products/accessories/it-robotics-workcellsimulator.jpg" alt="IT Robotics WorkCellSimulator" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>WorkCellSimulator | IT Robotics</h2>

									<p class="card-text" style="text-align:justify">Instead of testing new tasks directly in the real plant, WorkCellSimulator saves many stop-production days simulating the work cell in a 3D virtual environment.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".it-robotics-workcellsimulator-modal">More Information</button>

										<div class="modal fade it-robotics-workcellsimulator-modal" tabindex="-1" role="dialog" aria-labelledby="it-robotics-workcellsimulator-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">WorkCellSimulator</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>Starting from the high-level description of the process, WorkCellSimulator builds the virtual environment and simulate the behavior of the machines (e.g. bending machines, cartesian robots, CNC machines…) involved. lrWorkCellSimulator uses the most advanced artificial intelligence algorithms to automatically ensure no collisions and maximum efficiency paths. The simulation results are then translated in the controller program, supporting the most common robot and CN manufacturers, to be uploaded on robots and machines.</p>

															<p>The WorkCellSimulator framework has a modular architecture that adapts to the simulation of any type of industrial process. Its flexibility is extended by the scripting system that allows the customization of the process without requiring any special programming skills.</p>

															<p>WorkCellSimulator is the best choice in industrial automation, it facilitates programming at all levels: from the most complex work cells to the single machine.</p>

															<h3>The system integrator swiss army knife</h3>

															<p>The framework helps the system integrators to create tailored tools that are not available in the common robotic simulations suites. The tools will enable the operator, thanks to the AI powered task-planning engine, to dramatically reduce the process design and configuration times.</p>

															<p>WorkCellSimulator uses the most advanced artificial intelligence algorithms to autonomously define the production process, controlling one or more manipulator robots and the machinery in the work cell.</p>

															<p>The framework helps the machine-builders to improve their programming interfaces for their own machines passing from programming the task to simply configure it using brand new algorithms for task and motion planning.</p>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>