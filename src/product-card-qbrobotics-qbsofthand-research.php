

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/end-effectors/qbrobotics-qbsofthand-research.png" data-srcset="assets/img/products/end-effectors/qbrobotics-qbsofthand-research.png" alt="qbRobotics qbSofthand Research" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>qbSofthand (Research) | qbRobotics</h2>

									<p class="card-text" style="text-align:justify">qbSoftHand (Research) is an anthropomorphic robotic hand based on soft-robotics technology, flexible, adaptable and able to interact with the surrounding environment, objects and humans while limiting the risk of hurting the operators, spoiling the products to be handled, and damaging the robot itself.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".qbrobotics-qbsofthand-research-modal">More Information</button>

										<div class="modal fade qbrobotics-qbsofthand-research-modal" tabindex="-1" role="dialog" aria-labelledby="qbrobotics-qbsofthand-research-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">qbSofthand (Research)</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>The qb SoftHand is adaptable and can grasp different objects without any change in the control action, showing an unparalleled level of simplicity and flexibility.</p>

															<p>Thanks to its soft nature the hand by qbrobotics exploits the principles of synergies in a simple and intrinsically intelligent design that is not only safe in unexpected human-robot interaction, but also adaptable to grasp different objects without any change in the control action.</p>

															<p>The combination of these innovations results in a flexible prehensile device that can grasp a wide variety of objects. The single-motor actuation makes the hand plug-and-play and simple-to-control (one single motor requires one single control signal to close and open the whole hand) and affordable.</p>

															<p>The qb SoftHand Research has been designed for the educational sector and in particular for Research Institutes, Universities and testing laboratories.</p>

															<h3>Specs</h3>

															<table width="100%" style="text-align:left;">

																<tbody>

																	<tr>

																		<td width="50%">Flexible, Adaptive & Robust</td>

																		<td width="50%">19 anthropomorphic DOFs, one synergy, one motor</td>

																	</tr>

																	<tr>

																		<td width="50%">Dislocatable, self-healing finger joints</td>

																		<td width="50%">Grasp force 62N (pinch configuration)</td>

																	</tr>	

																	<tr>

																		<td width="50%">Nominal payload 1,7kg (pinch configuration)</td>

																		<td width="50%">From wide open to clenched fist in 1.1 s</td>

																	</tr>

																	<tr>

																		<td width="50%">USB & RS485 interfaces</td>

																		<td width="50%">ROS Compatible</td>

																	</tr>

																	<tr>

																		<td width="50%">UR+ Certified by Universal Robots</td>

																		<td width="50%">Weight: 500g</td>

																	</tr>

																	<tr>

																		<td width="50%">Feedback: motor position and motor current</td>

																		<td width="50%"></td>

																	</tr>

																</tbody>

															</table>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>