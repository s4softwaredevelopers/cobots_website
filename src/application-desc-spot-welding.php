<article class="portfolio-item">

							<div class="portfolio-image">

									<img data-toggle="modal" data-target=".spot-welding-modal"  src="assets/img/icons/applications/spot-welding.png" alt="Spot Welding">

							</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".spot-welding-modal">Spot Welding</a></h3>

								<div style="text-align:center">



										<div class="modal fade spot-welding-modal" tabindex="-1" role="dialog" aria-labelledby="spot-welding-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Spot Welding</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>A collaborative robot can assist with reduced production downtime and wasted material are among the advantages.</p> 

															<p>The following types of welding are possible with a collaborative robot:</p>
															<div style="padding:0 100px">
															<ul>
																<li>Spot</li> 
																<li>Ultrasound</li>
																<li>Plasma</li>
																<li>MIG</li>
																<li>TIG</li>
															</ul>
															</div>

															<p>By automating your welding you improve workplace safety that's allowing a robot to handle more dangerous tasks. Added advantages of a collaborative robot would be that if a robot comes into contact with a person, patented technology limits the forces at contact. UR can also be programmed to o operate in reduced mode when a human enters the robots work area and resume full speed when the person leaves.</p> 
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>

							</div>

						</article>