<article class="portfolio-item">

							<div class="portfolio-image">

									<img data-toggle="modal" data-target=".cnc-modal"  src="assets/img/icons/applications/cnc.png" alt="CNC">

							</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".cnc-modal">CNC</a></h3>

								<div style="text-align:center">



										<div class="modal fade cnc-modal" tabindex="-1" role="dialog" aria-labelledby="cnc-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">CNC</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>A collaborative robot will allow you to maximize efficiency, increase quality, optimize production and improve performance on your CNC machine.</p> 

															<p>Universal Robots can be used in most CNC applications. The space-saving CNC robot can be programmed quickly and be used with different machines and peripheral systems such as vision guidance.</p>
															<p>Having CNC automation will improve speed, precision, reliability as well as the quality of the output. It is important to note that a CNC robot arm can withstand significant environmental impact and changes in temperature, however, the collaborative robot would need protection when working in corrosive liquid environments. Industrial robot covers from Robo-Gear give full coverage and can be custom matched to your application.</p>

															<p>A benefit of the installation of a collaborative robot will improve speed while reducing the risk of an injury which is associated with human operators working in close proximity to a CNC machine. A collaborative robot can relieve you workers from ergonomically unfavourable repetitive tasks. With a collaborative robot being installed, one can free up your work force for them to focus on more important tasks as well as less dangerous tasks.</p>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>

							</div>

						</article>