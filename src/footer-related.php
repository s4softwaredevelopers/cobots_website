<div class="content-wrap">
	<div class="container clearfix">
		<div class="heading-block">
			<h1 style="color: #333">People also search for</h1>
		</div>
		<div class="portfolio grid-container portfolio-masonry clearfix">
			<?php include("brand-card-universal-robots-2.php");?>	
			<?php include("brand-card-mir-2.php");?>	
			<?php include("brand-card-it-robotics-2.php");?>	
			<?php include("brand-card-onrobot-2.php");?>	
		</div>
	</div>
</div>