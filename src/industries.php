<!DOCTYPE html>

<html dir="ltr" lang="en-US" prefix="og: http://ogp.me/ns#">

<head>



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="Cobots (Pty) Ltd." />

	<meta name="description" content="Collaborative robots are exceptionally useful in most, if not all, industries. Find out which industries we are most active in.">

	<meta name="robots" content="index, follow">

	<?php include ("favicon.php");?>


	<?php include ("stylesheets.php");?>

	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>Industries that utilise collaborative robots | Cobots (Pty) Ltd.</title>

	<!--OPENGRAPH-->

	<meta property="og:title" content="Industries that utilise collaborative robots | Cobots (Pty) Ltd." />

	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://www.cobots.co.za/industries" />

	<meta property="og:image" content="https://www.cobots.co.za/assets/img/home/cobots-featured-image.png" />

		<meta property="og:image:type" content="image/png" />

		<meta property="og:image:width" content="1200" />

		<meta property="og:image:height" content="630" />

		<meta property="og:image:alt" content="Welcome to Cobots (Pty) Ltd. | Industries" />



</head>



<body class="stretched">


	<div id="wrapper" class="clearfix">



		<?php require("header.php"); ?>

		<section id="page-title" class="page-title-dark page-title-center" style="background-image: url('https://www.cobots.co.za/assets/img/misc/industries-call-to-action-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">



			<div class="container clearfix">

				<h1 style="color:#fff !important">Industries</h1>

			</div>



		</section>


		<section id="content" style="background-color: #ffffff">

	

			<div class="content-wrap">

				<div id="portfolio" class="container clearfix">

					<div class="portfolio grid-container portfolio-5 portfolio-masonry clearfix app-icon-row" style="margin-bottom:20px;">



						<article class="portfolio-item">

							<div class="portfolio-image">

									<img src="https://www.cobots.co.za/assets/img/icons/industries/aerospace.png" alt="Aerospace">

							</div>

							<div class="portfolio-desc">

								<h3>Aerospace</h3>
							</div>

						</article>

						

						<article class="portfolio-item">

							<div class="portfolio-image">

									<img src="https://www.cobots.co.za/assets/img/icons/industries/agriculture.png" alt="Agriculture">

							</div>

							<div class="portfolio-desc">

								<h3>Agriculture</h3>

							</div>

						</article>

						

						<article class="portfolio-item">

							<div class="portfolio-image">

									<img src="https://www.cobots.co.za/assets/img/icons/industries/automotive.png" alt="Automotive">

							</div>

							<div class="portfolio-desc">

								<h3>Automotive</h3>

							</div>

						</article>

						

						<article class="portfolio-item">

							<div class="portfolio-image">

									<img src="https://www.cobots.co.za/assets/img/icons/industries/bio-medical.png" alt="Bio-medical">

							</div>

							<div class="portfolio-desc">

								<h3>Bio-medical</h3>

							</div>

						</article>

						

						<article class="portfolio-item">

							<div class="portfolio-image">

									<img src="https://www.cobots.co.za/assets/img/icons/industries/furniture.png" alt="Furniture">

							</div>

							<div class="portfolio-desc">

								<h3>Furniture</h3>

							</div>

						</article>

					</div>
					
				</div>

			</div>

			<div class="content-wrap" style="background-color:#f7f7f7">

				<div class="container clearfix">

						<div class="col_full nobottommargin center">

						<p style="text-align: justify; color: #333333;">Every industry has its distinctive challenges, however, that does not stop us from being able to assist you. Automation is essential in today's fast changing production environments. A  competitive advantage for companies would be one simple solution: robots.</p>
						
						<p style="text-align: justify; color: #333333;">Cobots can ensure you that there are many ways we can make your business work smarter, faster, safer and more efficient than before with all our products. Collaborative robots along with the various accessories can be adjusted and programmed to add value to any environment by taking over repetitive and high precision tasks. Cobots offers to consult on how you can automate and transform a collaborative into a business partner that will grow your bottom line well into the future. Across all industries collaborative robots provide automation, unparalleled simplicity and diverse application coverage.</p>
						</div>

				</div>

			</div>

			<div class="content-wrap">

				<div id="portfolio" class="container clearfix">


					<div class="portfolio grid-container portfolio-5 portfolio-masonry clearfix app-icon-row" style="margin-bottom:20px;">

						<article class="portfolio-item">

							<div class="portfolio-image">

									<img src="https://www.cobots.co.za/assets/img/icons/industries/metal-and-machining.png" alt="Metal and Machining">

							</div>

							<div class="portfolio-desc">

								<h3>Metal and Machining</h3>

							</div>

						</article>

						

						<article class="portfolio-item">

							<div class="portfolio-image">

									<img src="https://www.cobots.co.za/assets/img/icons/industries/pharma.png" alt="Pharma and Chemistry">

							</div>

							<div class="portfolio-desc">

								<h3>Pharma and Chemistry</h3>

							</div>

						</article>

						

						<article class="portfolio-item">

							<div class="portfolio-image">

									<img src="https://www.cobots.co.za/assets/img/icons/industries/plastics.png" alt="Plastics and Polymers">

							</div>

							<div class="portfolio-desc">

								<h3>Plastics and Polymers</h3>

							</div>

						</article>

						

						<article class="portfolio-item">

							<div class="portfolio-image">

									<img src="https://www.cobots.co.za/assets/img/icons/industries/science.png" alt="Scientific and Research">

							</div>

							<div class="portfolio-desc">

								<h3>Scientific and Research</h3>

							</div>

						</article>

						

						<article class="portfolio-item">

							<div class="portfolio-image">

									<img src="https://www.cobots.co.za/assets/img/icons/industries/technology.png" alt="Electronics and Technology">

							</div>

							<div class="portfolio-desc">

								<h3>Electronics and Technology</h3>

							</div>

						</article>

					</div>
				</div>
			</div>






			

			<div id="related">

				<?php include ("footer-related.php")?>
			</div>

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/misc/industries-call-to-action-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<h2 style="color: #fff; text-align:center">Cobots is your one-stop shop for all your automation needs. Contact us today!</h2>

						<div class="widget clearfix" style="text-align:center">

							<a href="https://www.cobots.co.za/contact" class="btn btn-contact">Contact Us</a>

						</div>

				</div>

			</div>

		</section>


		<?php require("footer.php"); ?>

</body>

</html>