<!DOCTYPE html>

<html dir="ltr" lang="en-US">

<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="Cobots (Pty) Ltd." />

	<meta name="description" content="This is a 404 error. You should not be seeing this, but the content you are looking for could not be found.">

	<meta name="robots" content="no-index, no-follow">

	<?php include ("favicon.php");?>

	<?php include ("stylesheets.php");?>

	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<title>Whoops! Looks like you've found our 404 Error page | Cobots (Pty) Ltd.</title>

</head>



<body class="stretched">

	<div id="wrapper" class="clearfix">

		<?php require("header.php"); ?>

		<section id="404-page-title" class="page-title-dark page-title-center" style="background-image: url('/assets/img/brands/universal-robots/circuit.jpg');background-size: cover;padding-top: 0px;padding-bottom: 0px;" data-bottom-top="background-position:0px 0px; padding 0 0 0 0 !important;" data-top-bottom="background-position:0px -300px;">
			<div class="container clearfix">
			<div class="row">

				<div class="col-sm-8">

					<h1 style="color:#333 !important; padding-top: 100px;">404 Error</h1>

					<span style="color:#333; padding-bottom: 100px;"><strong>These are not the droids you're looking for...</strong></span>

				</div>

				<div class="col-sm-4">

				<img src='assets/img/misc/not-the-droids.png' alt="Not these droids!!">

				</div>

			</div>
			</div>



		</section>

		<section id="content">

			<div class="content-wrap" style="background-color:#eeeeee">

				<div class="container clearfix">

							<div class="heading-block">

								<h1 style="color: #333">Have you seen these?</h1>

							</div>

							<div class="portfolio grid-container portfolio-masonry clearfix">

								<?php include("brand-card-mir-2.php");?>

								<?php include("brand-card-qbrobotics-2.php");?>

								<?php include("brand-card-robotiq-2.php");?>

								<?php include("brand-card-onrobot-2.php");?>

							</div>

				</div>

			</div>

			<div class="content-wrap" style="background-image: url('assets/img/brands/universal-robots/universal-robots-call-to-action-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<h2 style="color: #fff; text-align:center">Contact us today to help you find what you are looking for.</h2>

						<div class="widget clearfix" style="text-align:center">

							<button type="button" class="btn btn-contact">Contact Us</button>

						</div>

				</div>

			</div>

		</section>
		<?php require("footer.php"); ?>

</body>

</html>