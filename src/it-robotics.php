<!DOCTYPE html>

<html dir="ltr" lang="en-US" prefix="og: http://ogp.me/ns#">

<head>



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="Cobots (Pty) Ltd." />

	<meta name="description" content="IT+Robotics designs and develops highly innovative solutions for robotics and industrial automation, with vision systems and software solutions.">

	<meta name="robots" content="index, follow">

	<?php include ("favicon.php");?>


	
	<?php include ("stylesheets.php");?>

	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>Brands | IT+Robotics | Cobots (Pty) Ltd.</title>

	<!--OPENGRAPH-->

	<meta property="og:title" content="Brands | IT+Robotics | Cobots (Pty) Ltd." />

	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://www.cobots.co.za/it-robotics" />

	<meta property="og:image" content="https://www.cobots.co.za/assets/img/home/cobots-featured-image.png" />

		<meta property="og:image:type" content="image/png" />

		<meta property="og:image:width" content="1200" />

		<meta property="og:image:height" content="630" />

		<meta property="og:image:alt" content="Welcome to Cobots (Pty) Ltd. | Brands | IT Robotics" />

</head>



<body class="stretched">



	
	<div id="wrapper" class="clearfix">



		<?php require("header.php"); ?>


		<script type="text/javascript" src="assets/js/lazyload.js"></script>

		
		<section id="brand-page-title" class="page-title-dark page-title-center" style="background-image: url('https://www.cobots.co.za/assets/img/brands/it-robotics/it-robotics-title-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">



			<div class="container clearfix">

				<h1 style="color:#333 !important">Introducing IT Robotics</h1>

				<span style="color:#333"><strong>IT+Robotics designs and develops highly innovative solutions for robotics and industrial automation</strong></span>

			</div>



		</section>




		
		<section id="content">

			



			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/brands/it-robotics/it-robotics-intro-background.png'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<div class="col_full nobottommargin center">

						<p style="text-align: justify; color: #333333;">IT+Robotics teams' mission is to increase the flexibility of industrial processes by transforming the results of academic research into cutting-edge industrial solutions.</p>

						<p style="text-align: justify; color: #333333;">Their research and Development department is in constant contact with the most advanced research institutes in Italy and aboard to create and improve technologies that IT+Robotics will transfer to companies thanks to its deep knowledge of the industrial sector.  IT+Robotics in these years has grown and established itself as one of the leaders in the industrial vision sector, in Italy and abroad, exporting its made in Italy technology to several foreign countries. IT+Robotics deals with machine vision applied to quality control and robot guidance. Moreover, IT+Robotics focuses on offline programming software for work cells and machinery.</p>

						</div>	

				</div>

			</div>

			





			<div class="content-wrap"  style="background-color:#f7f7f7">

				<div class="container clearfix">

					<div class="heading-block" style="padding-top: 40px;" data-animate="fadeInLeft">

						<h1 style="color: #333">Applications</h1>

					</div>



					<div id="portfolio" class="portfolio grid-container portfolio-6 portfolio-masonry clearfix">



						<?php include("application-desc-bin-picking.php");?>

						

						<?php include("application-desc-inventory-organisation.php");?>

						

						<?php include("application-desc-motion-detection.php");?>

						<?php include("application-desc-offline-programming.php");?>

						<?php include("application-desc-part-tracking.php");?>

						<?php include("application-desc-vision-inspection.php");?>

					</div>
				</div>

			</div>

			<div class="content-wrap" style="background-color:#eeeeee">

				<div class="container clearfix">

							<div class="heading-block" style="padding-top: 40px;" data-animate="fadeInLeft">

								<h1 style="color: #333">Products</h1>

							</div>

							

							<?php include("product-card-it-robotics-eyetpick.php");?>

							

							<?php include("product-card-it-robotics-eyetinspect.php");?>

							

							<?php include("product-card-it-robotics-workcellsimulator.php");?>

				</div>

			</div>
			<div id="related">
				<?php include ("footer-related-alternate.php")?>
			</div>

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/brands/it-robotics/it-robotics-call-to-action-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<h2 style="color: #fff; text-align:center">Contact us today to add accessories and software from IT Robotics to your automated production line.</h2>

						<div class="widget clearfix" style="text-align:center">

							<a href="https://www.cobots.co.za/contact" class="btn btn-contact">Contact Us</a>

						</div>

				</div>

			</div>

		</section>


		<?php require("footer.php"); ?>

</body>

</html>