<!DOCTYPE html>

<html dir="ltr" lang="en-US" prefix="og: http://ogp.me/ns#">

<head>



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="Cobots (Pty) Ltd." />

	<meta name="description" content="The applications for collaborative robots are endless, you are only hindered by your imagination. Take a look at some of the applications available to you.">

	<meta name="robots" content="index, follow">

	<?php include ("favicon.php");?>	

	<?php include ("stylesheets.php");?>
	
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>Applications for collaborative robots | Cobots (Pty) Ltd.</title>

	<!--OPENGRAPH-->

	<meta property="og:title" content="Applications for collaborative robots | Cobots (Pty) Ltd." />

	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://www.cobots.co.za/applications" />

	<meta property="og:image" content="https://www.cobots.co.za/assets/img/home/cobots-featured-image.png" />

		<meta property="og:image:type" content="image/png" />

		<meta property="og:image:width" content="1200" />

		<meta property="og:image:height" content="630" />

		<meta property="og:image:alt" content="Welcome to Cobots (Pty) Ltd. | Applications" />

</head>



<body class="stretched">

	<div id="wrapper" class="clearfix">



		<?php require("header.php"); ?>

		<section id="page-title" class="page-title-dark page-title-center" style="background-image: url('https://www.cobots.co.za/assets/img/misc/applications-title-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">



			<div class="container clearfix">

				<h1 style="color:#fff !important">Robot Applications</h1>

				<span style="color:#fff"><strong>Using our robots, accessories, software and end-effectors you'll be able to accomplish limitless applications. Find a few of them listed below.</strong></span>

			</div>



		</section>

		<section id="content">
			
			<div class="content-wrap"  >

				<div id="portfolio" class="container clearfix">

					<div class="portfolio grid-container portfolio-6 portfolio-masonry clearfix app-icon-row" style="margin-bottom:20px;">



						<?php include("application-desc-assembly.php");?>

						

						

						<?php include("application-desc-bin-picking.php");?>

						

						

						<?php include("application-desc-cnc.php");?>

						

						

						<?php include("application-desc-export-programs.php");?>

						

						

						<?php include("application-desc-gluing.php");?>

						

						

						<?php include("application-desc-injection-molding.php");?>

						

					</div>

					<div class="portfolio grid-container portfolio-6 portfolio-masonry clearfix app-icon-row" style="margin-bottom:20px;">



						<?php include("application-desc-inventory-organisation.php");?>

						

						

						<?php include("application-desc-lab-testing.php");?>

						

						

						<?php include("application-desc-machine-tending.php");?>

						

						

						<?php include("application-desc-motion-detection.php");?>

						

						

						<?php include("application-desc-offline-programming.php");?>

						

						

						<?php include("application-desc-palletising.php");?>

						

					</div>

					<div class="portfolio grid-container portfolio-6 portfolio-masonry clearfix app-icon-row" style="margin-bottom:20px;">



						<?php include("application-desc-part-tracking.php");?>

						

						

						<?php include("application-desc-pick-and-place.php");?>

						

						

						<?php include("application-desc-quality-testing.php");?>

						

						

						<?php include("application-desc-robot-accuracy.php");?>

						

						

						<?php include("application-desc-robot-library.php");?>

						

						

						<?php include("application-desc-spot-welding.php");?>

						

					</div>

					<div class="portfolio grid-container portfolio-6 portfolio-masonry clearfix app-icon-row" style="margin-bottom:20px;">



						<?php include("application-desc-surface-finishing.php");?>

						

						

						<?php include("application-desc-vision-inspection.php");?>

						

						

						<?php include("application-desc-infinite-possibilities.php");?>

						

					</div>

				</div>

			</div>
			<div id="related">
				<?php include ("footer-related.php")?>
			</div>

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/misc/applications-call-to-action-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<h2 style="color: #fff; text-align:center">Integrated automation assistance is one click away. Contact us today!</h2>

						<div class="widget clearfix" style="text-align:center">

							<a href="https://www.cobots.co.za/contact" class="btn btn-contact">Contact Us</a>

						</div>

				</div>

			</div>

		</section>


		<?php require("footer.php"); ?>

</body>

</html>