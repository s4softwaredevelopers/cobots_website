

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/robots/ur10.png" data-srcset="assets/img/products/robots/ur10.png" alt="UR10" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>UR10 | Universal Robots</h2>

									<p class="card-text" style="text-align:justify">The Universal Robots UR10 is our largest collaborative industrial robot arm, designed for bigger tasks where precision and reliability are still of paramount importance. With the UR10 collaborative industrial robot arm, you can automate processes and tasks with payloads that weigh up to 10 kg (22 lbs).</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".universal-robot-ur10-modal">More Information</button>

										<div class="modal fade universal-robot-ur10-modal" tabindex="-1" role="dialog" aria-labelledby="universal-robot-ur10-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">UR10</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<table width="100%">
																<tbody>
																	<tr><td><strong>Performance</strong></td></tr>
																	<tr>
																		<td width="30%">Power Consumption</td>
																		<td width="70%">Min 90W, Typical 250W, Max 500W</td>
																	</tr>
																	<tr>
																		<td width="30%">Collaboration Operation</td>
																		<td width="70%">15 advanced adjustable safety functions. T&#252;V NORD Approved Safety Function. Tested in accordance with: EN ISO 13849:2008 PL d</td>
																	</tr>
																	<tr><td><strong>Specification</strong></td></tr>
																	<tr>
																		<td width="30%">Payload</td>
																		<td width="70%">10 kg/22 lbs</td>
																	</tr>
																	<tr>
																		<td width="30%">Reach</td>
																		<td width="70%">1300 mm/ 51.2 in</td>
																	</tr>
																	<tr>
																		<td width="30%">Degrees of freedom</td>
																		<td width="70%">6 rotating joints</td>
																	</tr>
																	<tr>
																		<td width="30%">Programming</td>
																		<td width="70%">Polyscope graphical user interface on 12 inch touchscreen with mounting</td>
																	</tr>
																	
																</tbody>
															</table>
															<span class="btn btn-primary"><a href="https://www.cobots.co.za/docs/ur10-technical-specifications.pdf" target="blank" style="color:#ffffff !important">View UR10 technical specifications</a></span>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>

