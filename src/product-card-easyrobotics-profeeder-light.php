
							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="https://www.cobots.co.za/assets/img/products/accessories/easyrobotics-profeeder-light.png" data-srcset="https://www.cobots.co.za/assets/img/products/accessories/easyrobotics-profeeder-light.png" alt="Profeeder Light" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>Profeeder Light | EasyRobotics</h2>

									<p class="card-text" style="text-align:justify">ProFeeder Light forms part of Stage 1 automation as it increases efficiency and flexibility in production and making it possible to run a small series production unmanned. The ProFeeder Light is ideal for urgent orders or additional orders.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".easyrobotics-profeeder-light-modal">More Information</button>

										<div class="modal fade easyrobotics-profeeder-light-modal" tabindex="-1" role="dialog" aria-labelledby="easyrobotics-profeeder-light-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Profeeder Light</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body modal-list">
														<ul>	
															<li>ROI: 6-8 months</li>

															<li>Operation of parts tray: Manual change of parts</li>

															<li>Mobility: Easily Removable</li>

															<li>Series size:  Small series</li>
															
															<li>Number of part trays: 1</li>		

															<li>Production:  An “extra-hand”</li>	
															
															<li>Dimensions: 950x860x1125mm</li>	
														</ul>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>