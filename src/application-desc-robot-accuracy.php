<article class="portfolio-item">

							<div class="portfolio-image">

									<img data-toggle="modal" data-target=".robot-accuracy-modal"  src="assets/img/icons/applications/robot-accuracy.png" alt="Robot Accuracy">

							</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".robot-accuracy-modal">Robot Accuracy</a></h3>

								<div style="text-align:center">



										<div class="modal fade robot-accuracy-modal" tabindex="-1" role="dialog" aria-labelledby="robot-accuracy-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Robot Accuracy</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>Robot Accuracy is achieved through calibrating your collaborative robot's arm to improve accuracy and production results. The calibration improves the accuracy of robots which have been programmed offline.</p>														</div>

													</div>

												</div>

											</div>

										</div>

								</div>

							</div>

						</article>