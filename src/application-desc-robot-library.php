<article class="portfolio-item">

							<div class="portfolio-image">

									<img data-toggle="modal" data-target=".robot-library-modal"  src="assets/img/icons/applications/robot-library.png" alt="Robot Library">

							</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".robot-library-modal">Robot Library</a></h3>

								<div style="text-align:center">



										<div class="modal fade robot-library-modal" tabindex="-1" role="dialog" aria-labelledby="robot-library-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Robot Library</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>RoboDK has an extensive library of industrial robot arms, external axes and tools from over 30 different robot manufacturers. The Robot Library enables the use of any robot for any application such as:</p>
															<div style="padding:0 100px">
															<ul>
																<li>Machining</li>
																<li>Welding</li>
																<li>Cutting</li>
																<li>Painting</li>
																<li>Inspection</li>
																<li>Deburring</li> 
															</ul>
															</div>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>

							</div>

						</article>