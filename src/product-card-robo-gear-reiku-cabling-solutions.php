

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/accessories/robo-gear-reiku-cabling-solutions.png" data-srcset="assets/img/products/accessories/robo-gear-reiku-cabling-solutions.png"  alt="Robo-Gear Reiku Cabling" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>Robot Dress-out | Robo-Gear</h2>

									<p class="card-text" style="text-align:justify">Robo-Gear offers the full line of Reiku Cable Management Solutions.</p>

									<p class="card-text" style="text-align:justify">Robo-Gear works to offer you the best in robot dress pack options from Reiku. Conduit, clamps, trumpets, springs… all accessories needed to properly dress and protect sensitive equipment.</p>

								</div>



							</div>

