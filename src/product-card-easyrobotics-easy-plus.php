
							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="https://www.cobots.co.za/assets/img/products/software/easyrobotics-easy-plus.png" data-srcset="https://www.cobots.co.za/assets/img/products/software/easyrobotics-easy-plus.png" alt="Easy+ Software" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>Easy+ Software | EasyRobotics</h2>

									<p class="card-text" style="text-align:justify">EASYplus is a UR+plugin for Universal Robots that gives you a swift and effortless start to use ProFeeder products as well as the ER5 Mobile Cobot platform. The software is advantageous to those running low volume and high mix series.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".easyrobotics-easy-plus-modal">More Information</button>

										<div class="modal fade easyrobotics-easy-plus-modal" tabindex="-1" role="dialog" aria-labelledby="easyrobotics-easy-plus-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Easy+ Software</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body modal-list">
														<ul>	
															<li>Accessible for all Universal Robots (UR3,UR5 & UR10)</li>

															<li>Rapid changing time of parts in production</li>

															<li>Decreases programming time</li>

															<li>Release resources for other tasks than programming</li>
															
															<li>No skills in programming needed</li>		

														</ul>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>