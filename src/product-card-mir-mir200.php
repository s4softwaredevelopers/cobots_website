

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/robots/mir200.png" data-srcset="assets/img/products/robots/mir200.png" alt="MiR200" align="middle" style="padding:20px 0">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>MiR200 | MiR</h2>

									<p class="card-text" style="text-align:justify">The MiR200™ is a safe, cost-effective mobile robot that quickly automates your internal transportation and logistics. The robot optimizes workflows, freeing staff resources so you can increase productivity and reduce costs.</p>

									

										<button class="btn btn-primary" data-toggle="modal" data-target=".mir-mir200-modal">More Information</button>

										<div class="modal fade mir-mir200-modal" tabindex="-1" role="dialog" aria-labelledby="mir-mir200-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">MiR200™</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>The highly flexible MiR200™ autonomously transports up to 200 kg (440 lbs). It can be mounted with customized top modules such as bins, racks, lifts, conveyors or even a collaborative robot arm—whatever your application demands. Top modules are easy to change so the robot can be redeployed for different tasks.</p>

															<p>The MiR200™ robot safely maneuvers around people and obstacles, through doorways and in and out of elevators. You can download CAD files of the building directly to the robot, or program it with the simple, web-based interface that requires no prior programming experience. The robot’s mission can be easily adapted using a smartphone, tablet or computer connected to the network.</p>

															<p>With built-in sensors and cameras and sophisticated software, the MiR200™ can identify its surroundings and take the most efficient route to its destination, safely avoiding obstacles and people. Without the need to alter your facility with expensive, inflexible wires or sensors, the robot offers a fast return on investment, with payback in as little as a year.</p>

															<h3>Specs</h3>

															<table width="100%" style="text-align:left;">

																<tbody>

																	<tr>

																		<td width="50%"><strong>Length</strong></td>

																		<td width="50%">890mm</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Width</strong></td>

																		<td width="50%">580mm</td>

																	</tr>	

																	<tr>

																		<td width="50%"><strong>Height</strong></td>

																		<td width="50%">352mm</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Weight (without load)</strong></td>

																		<td width="50%">67kg</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Load Surface</strong></td>

																		<td width="50%">600mmx800mm</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Robot Payload</strong></td>

																		<td width="50%">200kg (maximum 5% incline)</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Certifications</strong></td>

																		<td width="50%">ESD certified</td>

																	</tr>

																</tbody>

															</table>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>
