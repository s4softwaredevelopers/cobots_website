<!DOCTYPE html>

<html dir="ltr" lang="en-US" prefix="og: http://ogp.me/ns#">

<head>



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="Cobots (Pty) Ltd." />

	<meta name="description" content="Different end-effectors can completely change the applications of your collaborative robot. Take a look at the variety of end-effectors we have on offer.">

	<meta name="robots" content="index, follow">

	<?php include ("favicon.php");?>


	<?php include ("stylesheets.php");?>

	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>End-effectors for collaborative robots | Cobots (Pty) Ltd.</title>

	<!--OPENGRAPH-->

	<meta property="og:title" content="End-effectors for collaborative robots | Cobots (Pty) Ltd." />

	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://www.cobots.co.za/end-effectors" />

	<meta property="og:image" content="https://www.cobots.co.za/assets/img/home/cobots-featured-image.png" />

		<meta property="og:image:type" content="image/png" />

		<meta property="og:image:width" content="1200" />

		<meta property="og:image:height" content="630" />

		<meta property="og:image:alt" content="Welcome to Cobots (Pty) Ltd. | End-effectors" />

</head>



<body class="stretched">

	<div id="wrapper" class="clearfix">



		<?php require("header.php"); ?>

		<script type="text/javascript" src="assets/js/lazyload.js"></script>


		<section id="page-title" class="page-title-dark page-title-center" style="background-image: url('https://www.cobots.co.za/assets/img/misc/end-effectors-title-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">



			<div class="container clearfix">

				<h1>Our End-Effectors</h1>

			</div>



		</section>



		<section id="content">

			<div class="content-wrap" style="background-color:#eeeeee">

				<div class="container clearfix">

							<?php include("product-card-robotiq-adaptive-grippers.php");?>

							

							<?php include("product-card-robotiq-3-finger-adaptive-gripper.php");?>

							

							<?php include("product-card-onrobot-rg2-gripper.php");?>

							

							<?php include("product-card-onrobot-rg6-gripper.php");?>

							

							<?php include("product-card-onrobot-dual-gripper.php");?>

							

							<?php include("product-card-onrobot-rg2-ft.php");?>

							

							<?php include("product-card-onrobot-gecko-gripper.php");?>

							

							<?php include("product-card-onrobot-polyskin-tactile-gripper.php");?>

							

							<?php include("product-card-qbrobotics-qbsofthand-research.php");?>

				</div>

			</div>

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/misc/end-effectors-call-to-action-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<h2 style="color: #fff; text-align:center">Include a compelling call-to-action. We are reliable, experienced etc etc. Contact us today!</h2>

						<div class="widget clearfix" style="text-align:center">

							<a href="https://www.cobots.co.za/contact" class="btn btn-contact">Contact Us</a>

						</div>

				</div>

			</div>

		</section>

		<?php require("footer.php"); ?>

</body>

</html>