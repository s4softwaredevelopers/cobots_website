

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/end-effectors/onrobot-hex-torque-sensor.png" data-srcset="assets/img/products/end-effectors/onrobot-hex-torque-sensor.png" alt="Onrobot Hex Torque Sensor" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>Hex 6-axis Force/Torque Sensor | OnRobot</h2>

									<p class="card-text" style="text-align:justify">Optoforce technology that is currently available in HEX-E/High-precision and HEX-H/Low-deformation models.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".onrobot-hex-torque-sensor-modal">More Information</button>

										<div class="modal fade onrobot-hex-torque-sensor-modal" tabindex="-1" role="dialog" aria-labelledby="onrobot-hex-torque-sensor-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Hex 6-axis Force/Torque Sensor</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>OnRobot 6-axis force torque sensors provide 6 degrees of freedom force and torque measurement. Our sensors are designed to fit most of the currently used industrial robot arms. Integration with various available interfaces is simple. Common applications are force control devices, teach-in activities and crash detection, but the sensors can be used next to end-effectors in case of grinding, polishing or deburring tools.</p>

															<h3>Features</h3>

															<table width="100%" style="text-align:left;">

																<tbody>

																	<tr>

																		<td width="30%"><strong>Comprehensive software package</strong></td>

																		<td width="70%">20% shorter integration time</td>

																	</tr>

																	<tr style="background-color:#f7f7f7">

																		<td width="30%"><strong>Easy-to-use graphical user interface<strong></td>

																		<td width="70%">It takes only 15 minutes to automate complex precision tasks</td>

																	</tr>

																	<tr>

																		<td width="30%"><strong>Pre-programmed force sensing applications</strong></td>

																		<td width="70%">No programming skill is needed | Applications: enter pointing, insertion, hand guiding, path recording</td>

																	</tr>

																	<tr style="background-color:#f7f7f7">

																		<td width="30%"><strong>New automation possibilities</strong></td>

																		<td width="70%">Automate tasks that would otherwise require the dexterity of the human hand</td>

																	</tr>

																	<tr>

																		<td width="30%"><strong>Keeps constant force while moving</strong></td>

																		<td width="70%">Automate precise tasks and set new quality standards in your production</td>

																	</tr>



																</tbody>

															</table>
															<p style="text-align:center">Adds the sense of touch to your robot</p>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>

