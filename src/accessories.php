<!DOCTYPE html>

<html dir="ltr" lang="en-US" prefix="og: http://ogp.me/ns#">

<head>



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="Cobots (Pty) Ltd." />

	<meta name="description" content="Welcome to our accessories page, here you will find all of our accessories relating to collaborative robots! Browse through the selection to extend your Cobot.">

	<meta name="robots" content="index, follow">

	<?php include ("favicon.php");?>

	<?php include ("stylesheets.php");?>	

	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>Accessories for collaborative robots | Cobots (Pty) Ltd.</title>

	<!--OG DATA PUSH-->

	<meta property="og:title" content="Accessories for collaborative robots | Cobots (Pty) Ltd." />

	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://www.cobots.co.za/accessories" />

	<meta property="og:image" content="https://www.cobots.co.za/assets/img/home/cobots-featured-image.png" />

		<meta property="og:image:type" content="image/png" />

		<meta property="og:image:width" content="1200" />

		<meta property="og:image:height" content="630" />

		<meta property="og:image:alt" content="Welcome to Cobots (Pty) Ltd. | Accessories" />

</head>



<body class="stretched">

	<div id="wrapper" class="clearfix">


		<?php require("header.php"); ?>

		<script type="text/javascript" src="assets/js/lazyload.js"></script>



		<section id="page-title" class="page-title-dark page-title-center" style="background-image: url('https://www.cobots.co.za/assets/img/misc/accessories-title-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">



			<div class="container clearfix">

				<h1>Our Accessories</h1>

			</div>



		</section>

		<section id="content">

			<div class="content-wrap" style="background-color:#eeeeee">

				<div class="container clearfix">

							<?php include("product-card-robotiq-wrist-camera.php");?>

							

							<?php include("product-card-robotiq-force-sensor.php");?>

							

							<?php include("product-card-it-robotics-eyetpick.php");?>

							

							<?php include("product-card-it-robotics-eyetinspect.php");?>

							

							<?php include("product-card-mir-pallet-lift.php");?>

							

							<?php include("product-card-mir-pallet-rack.php");?>

							

							<?php include("product-card-mir-charge.php");?>

							

							<?php include("product-card-onrobot-hex-torque-sensor.php");?>

							

							<?php include("product-card-onrobot-omd.php");?>

							

							<?php include("product-card-qbrobotics-qbmove.php");?>

							

							<?php include("product-card-qbrobotics-qbmove-advanced.php");?>

							

							<?php include("product-card-qbrobotics-flat-flange-addon.php");?>

							

							<?php include("product-card-qbrobotics-flat-flange.php");?>

							

							<?php include("product-card-qbrobotics-c-flange.php");?>

							

							<?php include("product-card-robo-gear-industrial-robot-cover.php");?>

							

							<?php include("product-card-robo-gear-disposable-cover.php");?>

							

							<?php include("product-card-robo-gear-accordion-lift-cover.php");?>

							

							<?php include("product-card-robo-gear-reiku-cabling-solutions.php");?>
							
							<?php include("product-card-easyrobotics-er5-mobile-cobot-platform.php");?>

							<?php include("product-card-easyrobotics-profeeder-light.php");?>

							<?php include("product-card-easyrobotics-profeeder.php");?>

							<?php include("product-card-easyrobotics-profeeder-multi.php");?>



				</div>

			</div>

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/misc/accessories-call-to-action-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<h2 style="color: #fff; text-align:center">Contact us to start your collaborative automation journey today!</h2>

						<div class="widget clearfix" style="text-align:center">

							<a href="https://www.cobots.co.za/contact" class="btn btn-contact">Contact Us</a>

						</div>

				</div>

			</div>

		</section>

		<?php require("footer.php"); ?>

</body>

</html>