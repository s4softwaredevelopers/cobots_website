<article class="portfolio-item">
	<div class="portfolio-image">

		<img src="https://www.cobots.co.za/assets/img/brands/onrobot/onrobot-brand-card.jpg" alt="OnRobot">

		<div class="portfolio-overlay"></div>

	</div>

									<div class="portfolio-desc" style="background-color:#fff">

										<h3><a href="https://www.cobots.co.za/onrobot">OnRobot</a></h3>

										<p>End-Effectors | Accessories</p>

												<a class="btn btn-primary" href="https://www.cobots.co.za/onrobot">View Page</a>

									</div>

</article>