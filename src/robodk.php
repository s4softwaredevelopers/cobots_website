<!DOCTYPE html>

<html dir="ltr" lang="en-US" prefix="og: http://ogp.me/ns#">

<head>



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="Cobots (Pty) Ltd." />

	<meta name="description" content="RoboDK provides simulation and Open License Programs for robots that can even be used offline.">

	<meta name="robots" content="index, follow">

	<?php include ("favicon.php");?>


	<?php include ("stylesheets.php");?>

	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>Brands | RoboDK | Cobots (Pty) Ltd.</title>

	<!--OPENGRAPH-->

	<meta property="og:title" content="Brands | RoboDK | Cobots (Pty) Ltd." />

	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://www.cobots.co.za/robodk" />

	<meta property="og:image" content="https://www.cobots.co.za/assets/img/home/cobots-featured-image.png" />

		<meta property="og:image:type" content="image/png" />

		<meta property="og:image:width" content="1200" />

		<meta property="og:image:height" content="630" />

		<meta property="og:image:alt" content="Welcome to Cobots (Pty) Ltd. | Brands | RoboDK" />

</head>



<body class="stretched">

	<div id="wrapper" class="clearfix">



		<?php require("header.php"); ?>

		<script type="text/javascript" src="assets/js/lazyload.js"></script>


		<section id="brand-page-title" class="page-title-dark page-title-center" style="background-image: url('assets/img/brands/robodk/robodk-title-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">



			<div class="container clearfix">

				<h1 style="color:#333 !important">Introducing RoboDK</h1>

				<span style="color:#333"><strong>Providing simulation and Open License Programs for robots</strong></span>

			</div>



		</section>




		<section id="content">

			

			

			
			<div style="background-color:#f7f7f7">

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/brands/robodk/robodk-intro-background.png'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<div class="col_full nobottommargin center">

						<p style="text-align: justify; color: #333333;">The software from RoboDK allows you to program any Industrial Robot offline with one simulation environment. With this software you are able to convert NC programs to robot programs, generate robot programs offline, export programs to your robot, callibrate your robot arm for improved accuracy, as well as create virtual environments to simulate robot applications. In addition, an extensive robot library is made available to you.</p>

						</div>	

				</div>

			</div>

			</div>

			<div class="content-wrap"  style="background-color:#ffffff">

				<div class="container clearfix">

					<div class="heading-block" style="padding-top: 40px;" data-animate="fadeInLeft">

						<h1 style="color: #333">Applications</h1>

					</div>

					<div id="portfolio" class="portfolio grid-container portfolio-5 portfolio-masonry clearfix">



						<article id="PickPlace" class="portfolio-item">

							<div class="portfolio-image">

									<img src="https://www.cobots.co.za/assets/img/icons/applications/cnc.png" alt="Robot Machining">

							</div>

							<div class="portfolio-desc">

								<h3>Robot Machining</h3>

							</div>

							<div style="text-align:center;">

							<button class="btn btn-primary" data-toggle="modal" data-target=".robot-machining-modal">More Information</button>

								<div class="modal fade robot-machining-modal" tabindex="-1" role="dialog" aria-labelledby="robot-machining-modal" aria-hidden="true">

									<div class="modal-dialog modal-lg">

										<div class="modal-body">

											<div class="modal-content">

												<div class="modal-header">

													<h2 class="modal-title">Robot Machining</h2>

													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

												</div>

												<div class="modal-body">

													<p>Use your robot arm like a 5-axis milling machine (CNC) or a 3D printer. Simulate and convert NC programs (G-code or APT-CLS files) to robot programs. RoboDK will automatically optimize the robot path, avoiding singularities, axis limits and collisions.</p>

												</div>

											</div>

										</div>

									</div>

								</div>

							</div>

						</article>

						

						<article id="application2" class="portfolio-item">

							<div class="portfolio-image">

									<img src="https://www.cobots.co.za/assets/img/icons/applications/offline-programming.png" alt="Offline Programming Software">

							</div>

							<div class="portfolio-desc">

								<h3>Offline Programming Software</h3>

							</div>

							<div style="text-align:center;">

							<button class="btn btn-primary" data-toggle="modal" data-target=".offline-programming-modal">More Information</button>

								<div class="modal fade offline-programming-modal" tabindex="-1" role="dialog" aria-labelledby="offline-programming-modal" aria-hidden="true">

									<div class="modal-dialog modal-lg">

										<div class="modal-body">

											<div class="modal-content">

												<div class="modal-header">

													<h2 class="modal-title">Offline Programming Software</h2>

													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

												</div>

												<div class="modal-body">

													<p>Simulation and Offline Programming of industrial robots has never been easier. Create your virtual environment to simulate your application in a matter of minutes.</p>

													<p>Easily generate robot programs offline for any robot controller. You don't need to learn vendor-specific programming anymore.</p>

												</div>

											</div>

										</div>

									</div>

								</div>

							</div>

						</article>

						

						<article id="application3" class="portfolio-item">

							<div class="portfolio-image">

									<img src="https://www.cobots.co.za/assets/img/icons/applications/robot-library.png" alt="Robot Library">

							</div>

							<div class="portfolio-desc">

								<h3>Robot Library</h3>

							</div>

							<div style="text-align:center;">

							<button class="btn btn-primary" data-toggle="modal" data-target=".robot-library-modal">More Information</button>

								<div class="modal fade robot-library-modal" tabindex="-1" role="dialog" aria-labelledby="robot-library-modal" aria-hidden="true">

									<div class="modal-dialog modal-lg">

										<div class="modal-body">

											<div class="modal-content">

												<div class="modal-header">

													<h2 class="modal-title">Robot Library</h2>

													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

												</div>

												<div class="modal-body">

													<p>Access an extensive library of industrial robot arms, external axes and tools from over 30 different robot manufacturers. Easily use any robot for any application, such as machining, welding, cutting, painting, inspection, deburring, and more!</p>

												</div>

											</div>

										</div>

									</div>

								</div>

							</div>

						</article>

						<article id="application4" class="portfolio-item">

							<div class="portfolio-image">

									<img src="https://www.cobots.co.za/assets/img/icons/applications/robot-accuracy.png" alt="Robot Accuracy">

							</div>

							<div class="portfolio-desc">

								<h3>Robot Accuracy</h3>

							</div>

							<div style="text-align:center;">

							<button class="btn btn-primary" data-toggle="modal" data-target=".robot-accuracy-modal">More Information</button>

								<div class="modal fade robot-accuracy-modal" tabindex="-1" role="dialog" aria-labelledby="robot-accuracy-modal" aria-hidden="true">

									<div class="modal-dialog modal-lg">

										<div class="modal-body">

											<div class="modal-content">

												<div class="modal-header">

													<h2 class="modal-title">Robot Accuracy</h2>

													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

												</div>

												<div class="modal-body">

													<p>Calibrate your robot arm to improve accuracy and production results. Run ISO9283 robot performance tests. Certify robots with a ballbar test.</p>

												</div>

											</div>

										</div>

									</div>

								</div>

							</div>

						</article>

						<article id="application4" class="portfolio-item">

							<div class="portfolio-image">

									<img src="https://www.cobots.co.za/assets/img/icons/applications/export-programs.png" alt="Export Programs">

							</div>

							<div class="portfolio-desc">

								<h3>Export Programs</h3>

							</div>

							<div style="text-align:center;">

							<button class="btn btn-primary" data-toggle="modal" data-target=".export-programs-modal">More Information</button>

								<div class="modal fade export-programs-modal" tabindex="-1" role="dialog" aria-labelledby="export-programs-modal" aria-hidden="true">

									<div class="modal-dialog modal-lg">

										<div class="modal-body">

											<div class="modal-content">

												<div class="modal-header">

													<h2 class="modal-title">Export Programs to your Robot</h2>

													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

												</div>

												<div class="modal-body" style="padding: 0 100px">

													<h3>RoboDK Post Processors support many robot controllers, including:</h3>


														<ul>

															<li>ABB RAPID (mod/prg)</li>

															<li>Fanuc LS (LS/TP)</li>

															<li>KUKA KRC/IIWA (SRC/java)</li>

															<li>Motoman Inform (JBI)</li>

															<li>Universal Robots (URP/script)</li>

															<li>and much more!</li>

														</ul>

												</div>

											</div>

										</div>

									</div>

								</div>

							</div>

						</article>

					</div>
				</div>

			</div>
			<div id="related">
				<?php include ("footer-related.php")?>
			</div>


			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/brands/robodk/robodk-call-to-action-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<h2 style="color: #fff; text-align:center">Contact us today to add software from RoboDK to your automated production line.</h2>

						<div class="widget clearfix" style="text-align:center">

							<a href="https://www.cobots.co.za/contact" class="btn btn-contact">Contact Us</button>

						</div>

				</div>

			</div>

		</section>


		<?php require("footer.php"); ?>

</body>

</html>