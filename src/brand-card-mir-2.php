<article class="portfolio-item">
	<div class="portfolio-image">
			<img src="https://www.cobots.co.za/assets/img/brands/mir/mir-brand-card.jpg" alt="MiR">
		<div class="portfolio-overlay"></div>

	</div>

	<div class="portfolio-desc" style="background-color:#fff">

		<h3><a href="https://www.cobots.co.za/mir">MiR</a></h3>

		<p>Robots | Accessories | Software</p>
				<a class="btn btn-primary" href="https://www.cobots.co.za/mir">View Page</a>


	</div>

</article>