		<footer id="footer" class="dark">
			<div class="container">
				<div class="footer-widgets-wrap clearfix" valign="center">



					<div class="col_one_third" align="center">



						<div class="widget clearfix">

							<address>

								<strong>Headquarters:</strong><br>

								The Workspace at the Club Retail, Shop 201,<br>

								Second Floor, Cnr 18th & Pinaster Ave, Pretoria<br>

							</address>

						</div>



					</div>



					<div class="col_one_third" align="center">

						<div class="widget clearfix">

							<strong>Contact Details</strong><br>

							012 764 7284<br>

							sales@cobots.co.za<br>

						</div>

					</div>



					<div class="col_one_third col_last" align="center">

						<div >

							<a class="btn btn-contact" href="https://www.cobots.co.za/contact">Contact Us</a>

						</div>

					</div>



				</div>


			</div>

			<div id="copyrights">



				<div class="container clearfix">

				<p style="text-align:center">Copyrights &copy; 2018 All Rights Reserved by Cobots (Pty) Ltd.</p>
				<p style="text-align:center;"><a style="color: #819081;" href="https://www.cobots.co.za/privacy-policy">Privacy Policy</a>     |     <a style="color: #819081;" href="https://www.cobots.co.za/terms-and-conditions">Terms and Conditions</a></p>

				</div>



			</div>


		</footer>


	</div>



	<div id="gotoTop" class="icon-angle-up"></div>


	<script src="assets/js/jquery.js"></script>

	<script src="assets/js/plugins.js"></script>



	<script src="assets/js/functions.js"></script>



</body>

</html>