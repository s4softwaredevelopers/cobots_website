<!DOCTYPE html>

<html dir="ltr" lang="en-US" prefix="og: http://ogp.me/ns#">

<head>



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="Cobots (Pty) Ltd." />

	<meta name="description" content="Universal Robots creates cost-effective, safe and flexible collaborative robots - making automation easier than ever!">

	<meta name="robots" content="index, follow">

	<?php include ("favicon.php");?>

	<?php include ("stylesheets.php");?>

	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>Brands | Universal Robots | Cobots (Pty) Ltd.</title>

	<!--OPENGRAPH-->

	<meta property="og:title" content="Brands | Universal Robots | Cobots (Pty) Ltd." />

	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://www.cobots.co.za/universal-robots" />

	<meta property="og:image" content="https://www.cobots.co.za/assets/img/home/cobots-featured-image.png" />

		<meta property="og:image:type" content="image/png" />

		<meta property="og:image:width" content="1200" />

		<meta property="og:image:height" content="630" />

		<meta property="og:image:alt" content="Welcome to Cobots (Pty) Ltd. | Brands | Universal Robots" />

</head>



<body class="stretched">

	<div id="wrapper" class="clearfix">



		<?php require("header.php"); ?>

		<script type="text/javascript" src="assets/js/lazyload.js"></script>


		<section id="brand-page-title" class="page-title-dark page-title-center" style="background-image: url('https://www.cobots.co.za/assets/img/brands/universal-robots/universal-robots-title-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">



			<div class="container clearfix">

				<h1 style="color:#333 !important">Introducing Universal Robots</h1>

				<span style="color:#333"><strong>Their cost-effective, safe and flexible collaborative robots are making automation easier than ever!</strong></span>

			</div>



		</section>

		<section id="content">

			

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/brands/universal-robots/ur-intro-background.png'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<div class="col_full nobottommargin center">

						<p style="text-align: justify; color: #333333;">The team at Universal Robots is dedicated to bringing safe, flexible, and easy-to-use 6-axis industrial robot arms to businesses of every size. The collaborative robots are developed to automate and streamline repetitive industrial processes, this enables employers to apply their workforce to more value added tasks. Subsequently, companies enjoy fewer inaccuracies while employees can enjoy a mopre dynamic and interesting workload.</p>

						<p style="text-align: justify; color: #333333;">Collaborative robots are helping bridge the gap in manufacturing operations, from small and medium sized companies to large multinationals. The collaborative robots are fitted with an end-effector, which is mounted on the robots arm to interact with parts and machines. These end-effectors range from suction cups and grippers for part-picking, to a spot-welding tool or paint sprayer. Your options are only limited by your imagination.</p>

						</div>	

				</div>

			</div>

			

			

			<div class="content-wrap"  style="background-color:#f7f7f7">

				<div class="container clearfix">

					<div class="heading-block" style="padding-top: 40px;" data-animate="fadeInLeft">

						<h1 style="color: #333">Applications</h1>

					</div>

					<div id="portfolio" class="portfolio grid-container portfolio-6 portfolio-masonry clearfix">



						<?php include("application-desc-pick-and-place.php");?>

						

						<?php include("application-desc-injection-molding.php");?>

						

						<?php include("application-desc-cnc.php");?>

						

						<?php include("application-desc-assembly.php");?>

						

						<?php include("application-desc-palletising.php");?>

						<?php include("application-desc-infinite-possibilities.php");?>

					</div>
				</div>

			</div>

			<div class="content-wrap" style="background-color:#eeeeee">

				<div class="container clearfix">

							<div class="heading-block" style="padding-top: 40px;" data-animate="fadeInLeft">

								<h1 style="color: #333">Products</h1>

							</div>

							<?php include("product-card-universal-robot-ur3.php");?>

							

							

							<?php include("product-card-universal-robot-ur5.php");?>

							

							

							<?php include("product-card-universal-robot-ur10.php");?>

							

							

							<?php include("product-card-universal-robot-ur3e.php");?>

							

							

							<?php include("product-card-universal-robot-ur5e.php");?>

							

							

							<?php include("product-card-universal-robot-ur10e.php");?>

				</div>

			</div>

			<div id="related">
				<?php include ("footer-related-alternate.php")?>
			</div>

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/brands/universal-robots/universal-robots-call-to-action-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<h2 style="color: #fff; text-align:center">Contact us today to add robots from Universal Robots to your automated production line.</h2>

						<div class="widget clearfix" style="text-align:center">

							<a href="https://www.cobots.co.za/contact" class="btn btn-contact">Contact Us</a>

						</div>

				</div>

			</div>

		</section>

		<?php require("footer.php"); ?>


</body>

</html>