

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/accessories/qbrobotics-qbmove-advanced.png" data-srcset="assets/img/products/accessories/qbrobotics-qbmove-advanced.png" alt="qbRobotics qbMove Advanced" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>qbMove Advanced | qbRobotics</h2>

									<p class="card-text" style="text-align:justify">qbActuators are based on variable stiffness actuation and present a wealth of potential uses due to their naturalness of motion, energy efficiency, speed, robustness and task adaptability. The qbMove is a single actuator, proposed in Robotic Kit comprised of multiple qbMoves and connector pieces for assembly and configuration in different robots, e.g. a snake, a hexapod, or a humanoid.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".qbrobotics-qbmove-advanced-modal">More Information</button>

										<div class="modal fade qbrobotics-qbmove-advanced-modal" tabindex="-1" role="dialog" aria-labelledby="qbrobotics-qbmove-advanced-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">qbMove Advanced</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>Our VSA, whose operation is similar to a pair of muscles, have the shape of cubes to facilitate modular assembly based on the needs of the end user. The qbmove is a single actuator, proposed in its Advanced version and sold in Robotic Kit that allow different assembly configurations (4 and 6 DOFs robotic arms, snake, hexapod, delta and humanoid).</p>

															<p>The qbmove Advanced represents the best of the state of the art of the VSA actuators and is proposed for the educational sector to research institutes and universities.</p>

															<h3>Specs</h3>

															<table width="100%" style="text-align:left;">

																<tbody>

																	<tr>

																		<td width="50%">USB & RS485 communication</td>

																		<td width="50%">Injection moulding nylon case</td>

																	</tr>

																	<tr>

																		<td width="50%">Nominal torque 5.5 Nm</td>

																		<td width="50%">Nominal speed 5.5 rad/s</td>

																	</tr>	

																	<tr>

																		<td width="50%">Nominal Voltage 24V</td>

																		<td width="50%">Variable stiffness range [0.5 – 83.5] Nm/rad</td>

																	</tr>

																	<tr>

																		<td width="50%">Active Rotation Angle +/- 180°</td>

																		<td width="50%">ROS compatible</td>

																	</tr>

																	<tr>

																		<td width="50%">Weight 0,45Kg</td>

																		<td width="50%">Dimensions 66x66x66mm</td>

																	</tr>

																</tbody>

															</table>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>

