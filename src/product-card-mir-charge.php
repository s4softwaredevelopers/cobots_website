

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/accessories/mir-charge.png" data-srcset="assets/img/products/accessories/mir-charge.png" alt="MiR Charge" align="middle" style="padding:25px 0">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>MiR Charge | MiR</h2>

									<p class="card-text" style="text-align:justify">Fully automatic charging station keeps your mobile robots powered and on the job. MiRCharge™ is a fully automatic charging station that lets MiR robots autonomously reload their batteries as needed. This small, non-intrusive charging station uses standard power and can be easily integrated into almost any environment.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".mir-charge-modal">More Information</button>

										<div class="modal fade mir-charge-modal" tabindex="-1" role="dialog" aria-labelledby="mir-charge-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">MiR Charge</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>Your MiR100™ or MiRHook™ robot will never run out of power in the middle of a job. MiRCharge™ is a fully automatic charging station that offers flexible deployment options to meet your needs.</p>

															<p>The robot autonomously returns to the charging station and connects when its battery falls under established limits. It can also be programmed to regularly recharge between deliveries, such as when it returns to a stockroom or depot, which can allow the robot to run nearly continuously. MiRCharge™ fully charges a MiR robot in about two hours. When multiple robots are deployed using MiRFleet™, the system evaluates battery levels and job loads, optimizing utilization and recharging to keep the fleet on the job around the clock.</p>

															<p>MiRCharge™ is easy and unobtrusive to install in your facility. It uses standard power outlets and its low profile and small footprint give you additional flexibility in how you power and deploy your mobile robot.</p>

															<h3>Specs</h3>

															<table width="100%" style="text-align:left;">

																<tbody>

																	<tr>

																		<td width="50%"><strong>Width</strong></td>

																		<td width="50%">580mm</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Height</strong></td>

																		<td width="50%">300mm</td>

																	</tr>	

																	<tr>

																		<td width="50%"><strong>Depth</strong></td>

																		<td width="50%">120mm</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Weight</strong></td>

																		<td width="50%">10.7kg</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Wall mounting</strong></td>

																		<td width="50%">To be mounted flush with floor</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Mounting height above floor</strong></td>

																		<td width="50%">4.5cm from floor to bottom edge</td>

																	</tr>

																</tbody>

															</table>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>
