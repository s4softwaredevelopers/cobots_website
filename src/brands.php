<!DOCTYPE html>

<html dir="ltr" lang="en-US" prefix="og: http://ogp.me/ns#">

<head>



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="Cobots (Pty) Ltd." />

	<meta name="description" content="At Cobots we are approved partners and endorse the following brands. Take a look to see what type of collaborative robots we can integrate into your flow-line.">

	<meta name="robots" content="index, follow">

	<?php include ("favicon.php");?>

	<?php include ("stylesheets.php");?>

	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>Brands that we endorse | Cobots (Pty) Ltd.</title>

	<!--OPENGRAPH-->

	<meta property="og:title" content="Brands that we endorse | Cobots (Pty) Ltd." />

	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://www.cobots.co.za/brands" />

	<meta property="og:image" content="https://www.cobots.co.za/assets/img/home/cobots-featured-image.png" />

		<meta property="og:image:type" content="image/png" />

		<meta property="og:image:width" content="1200" />

		<meta property="og:image:height" content="630" />

		<meta property="og:image:alt" content="Welcome to Cobots (Pty) Ltd. | Brands" />

</head>



<body class="stretched">

	<div id="wrapper" class="clearfix">



		<?php require("header.php"); ?>

		<section id="page-title" class="page-title-dark page-title-center"  style="background-image: url('https://www.cobots.co.za/assets/img/misc/brands-title-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">



			<div class="container clearfix">

				<h1 style="color:#fff !important">Brands</h1>

				<span style="color:#fff"><strong>At Cobots we have partnered with numerous brands to provide the most innovative collaborative solutions. All collaborative robots and accessories have the ability to be implemented across all industries.</strong></span>

			</div>



		</section>

		<section id="content" style="background-color: #ffffff">

			

			<div class="content-wrap">

				<div class="container clearfix">

						<div class="col_full nobottommargin center">

						<p style="text-align: justify; color: #333333;">For a collaborative robot to be fully effective and effiecient, it is essential that accessories are added. This eases the integration of automation within an operation. At Cobots we offer a variety of accessories from our international partners. Adding accessories from OnRobot, Robotiq, qbRobotics and IT-Robotics will assist with motion control as well as increasing safety when placing your robot amongst human workers.</p>
						<p style="text-align: justify; color: #333333;">All brands at Cobots can assist with various <a href="https://www.cobots.co.za/applications">applications</a> that are used throughout various <a href="https://www.cobots.co.za/industries">industries</a>. Each collaborative robot is tailored to your company's needs whether it be automating your CNC machines or a simple repetitive task such as Pick and Place. With our expertise in collaborative robots and the automation industry, you will feel at ease that the solution provided to you will benefit your operation at the highest level.</p>
						</div>
				</div>

			</div>

						

			<div class="content-wrap" style="background-color:#eeeeee">

				<div class="container clearfix">

							<div class="portfolio grid-container portfolio-3 portfolio-masonry clearfix">

								<?php include("brand-card-universal-robots-2.php");?>	

								<?php include("brand-card-mir-2.php");?>	

								<?php include("brand-card-it-robotics-2.php");?>	
								
								<?php include("brand-card-qbrobotics-2.php");?>	

								<?php include("brand-card-robodk-2.php");?>	

								<?php include("brand-card-robo-gear-2.php");?>	

								<?php include("brand-card-robotiq-2.php");?>	
								
								<?php include("brand-card-onrobot-2.php");?>
								
								<?php include("brand-card-easyrobotics.php");?>
								
							</div>

				</div>

			</div>

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/misc/brands-call-to-action-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<h2 style="color: #fff; text-align:center">We are reliable, experienced and are here to make your automation journey easier. Contact us today!</h2>

						<div class="widget clearfix" style="text-align:center">

							<a href="www.cobots.co.za/contact" class="btn btn-contact">Contact Us</a>

						</div>

				</div>

			</div>

		</section>
		

		<?php require("footer.php"); ?>

</body>

</html>