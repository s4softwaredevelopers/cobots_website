

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/robots/ur5.png" data-srcset="assets/img/products/robots/ur5.png" alt="UR5" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>UR5 | Universal Robots</h2>

									<p class="card-text" style="text-align:justify">Our iconic collaborative robots were built with versatility and adaptability in mind. Lightweight, easily programmable and highly customizable, the UR5 is designed to integrate seamlessly into any production facility regardless of industry, size or product nature.</p>										<button class="btn btn-primary" data-toggle="modal" data-target=".universal-robot-ur5-modal">More Information</button>

										<div class="modal fade universal-robot-ur5-modal" tabindex="-1" role="dialog" aria-labelledby="universal-robot-ur5-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">UR5</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<table width="100%">
																<tbody>
																	<tr><td><strong>Performance</strong></td></tr>
																	<tr>
																		<td width="30%">Power Consumption</td>
																		<td width="70%">Min 90W, Typical 150W, Max 325W</td>
																	</tr>
																	<tr>
																		<td width="30%">Collaboration Operation</td>
																		<td width="70%">15 advanced adjustable safety functions. T&#252;V NORD Approved Safety Function. Tested in accordance with: EN ISO 13849:2008 PL d</td>
																	</tr>
																	<tr><td><strong>Specification</strong></td></tr>
																	<tr>
																		<td width="30%">Payload</td>
																		<td width="70%">5 kg / 11 lbs</td>
																	</tr>
																	<tr>
																		<td width="30%">Reach</td>
																		<td width="70%">850 mm / 33.5 in</td>
																	</tr>
																	<tr>
																		<td width="30%">Degrees of freedom</td>
																		<td width="70%">6 rotating joints</td>
																	</tr>
																	<tr>
																		<td width="30%">Programming</td>
																		<td width="70%">Polyscope graphical user interface on 12 inch touchscreen with mounting</td>
																	</tr>
																	
																</tbody>
															</table>
															<span class="btn btn-primary"><a href="https://www.cobots.co.za/docs/ur5-technical-specifications.pdf" target="blank" style="color:#ffffff !important">View UR5 technical specifications</a></span>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>