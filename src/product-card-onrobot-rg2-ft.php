
							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/end-effectors/onrobot-rg2-ft.jpg" data-srcset="assets/img/products/end-effectors/onrobot-rg2-ft.jpg" alt="Onrobot RG2-FT Smart Gripper" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>RG2-FT - Smart Gripper with F/T Sensor | OnRobot</h2>

									<p class="card-text" style="text-align:justify">See and feel objects with intelligent gripping.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".onrobot-rg2-ft-modal">More Information</button>

										<div class="modal fade onrobot-rg2-ft-modal" tabindex="-1" role="dialog" aria-labelledby="onrobot-rg2-ft-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">RG2-FT | Smart Gripper with F/T Sensor</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>You can automate high precision assembly tasks with OnRobot RG2-FT. The inbuilt 6 axis F/T sensors at the fingertips provide extremely accurate gripping for better production quality.</p>

															<p><strong>Features</strong></p>

																<div class="modal-list">

																<ul>
																	<li>Gripper with F/T sensor at the fingertips</li>

																	<tli>Detects workpiece using proximity sensor</li>

																	<li>Detects risk of slipping before it happens</li>

																	<li>Precise and simple depth compensation</li>

																	<li>Integrated force-controlled insertion software</li>

																	<li>Force/torque sensor for ewnabling precise jobs and collaborative operations</li>

																</ul>

																</div>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>

