<article class="portfolio-item">

							<div class="portfolio-image">

									<img data-toggle="modal" data-target=".lab-testing-modal"  src="assets/img/icons/applications/lab-testing.png" alt="Lab Testing">

							</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".lab-testing-modal">Lab Testing</a></h3>

								<div style="text-align:center">



										<div class="modal fade lab-testing-modal" tabindex="-1" role="dialog" aria-labelledby="lab-testing-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Lab Testing</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>Within a laboratory, a collaborative robot can automate demanding research projects that require flexibility, efficient use of space and seamless integration of lab peripherals. </p>

															<p>With a collaborative robot, you are able to customize your system to deliver ultimate flexibility while providing 24/7 operation for time-sensitive testing and analysis. </p>


															<p>For consistency and predictable quality, a collaborative robot will consistently and repeatedly follow exact processes and pre-defined workflows with miniscule/minimum deviation, providing optimum conditions for study or analysis.</p>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>

							</div>

						</article>