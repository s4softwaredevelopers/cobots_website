
							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/accessories/it-robotics-eyetpick.jpg" data-srcset="assets/img/products/accessories/it-robotics-eyetpick.jpg" alt="IT Robotics eyeTpick" align="middle" style="padding: 50px 0">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>EyeT+Pick | IT Robotics</h2>

									<p class="card-text" style="text-align:justify">EyeT+Pick is the outstanding 3D device for random bin picking. EyeT+Pick allows a robot manipulator to collect objects randomly arranged within a container.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".it-robotics-eyetpick-modal">More Information</button>

										<div class="modal fade it-robotics-eyetpick-modal" tabindex="-1" role="dialog" aria-labelledby="it-robotics-eyetpick-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">EyeT+Pick</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>The use of vision systems brings many benefits with respect to conventional automation systems. One of the most important factors is the extreme flexibility with which various types of products can be handled using the same automation. Since the system collects the products directly from the containers in which they are placed, there is no need for mechanical systems dedicated to each single type of product. New product models can be defined directly by the operator using Smart Pick 3D Solid, the programming software supplied along with the system itself.</p>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>
