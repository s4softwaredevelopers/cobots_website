

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/end-effectors/onrobot-gecko-gripper.png" data-srcset="assets/img/products/end-effectors/onrobot-gecko-gripper.png" alt="Onrobot Gecko Gripper" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>Gecko Gripper | OnRobot</h2>

									<p class="card-text" style="text-align:justify">Attach like a gecko and automate more.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".onrobot-gecko-gripper-modal">More Information</button>

										<div class="modal fade onrobot-gecko-gripper-modal" tabindex="-1" role="dialog" aria-labelledby="onrobot-gecko-gripper-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Gecko Gripper</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>The nature inspired OnRobot gecko technology makes it possible to attach and  lift any kind of flat and smooth surfaces. Fast and easy gripping technology for Pick & Place application.</p>

															<p><strong>Features</strong></p>
																<div class="modal-list">
																<ul>
																	<li>Grabs with gecko-style adhesive</li>

																	<li>Picks up flat objects without air system</li>

																	<li>Can lift solid or porous objects</li>

																	<li>Instantaneous gripping</li>

																	<li>Integrated load sensor for precise gripping</li>

																</ul>
																</div>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>

