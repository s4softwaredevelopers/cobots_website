

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/accessories/mir-fleet.jpg" data-srcset="assets/img/products/accessories/mir-fleet.jpg" alt="MiR Fleet" align="middle" style="padding:15px 0">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>MiR Fleet | MiR</h2>

									<p class="card-text" style="text-align:justify">Easily manage a fleet of mobile robots from a user-friendly, web-based interface. Optimize your internal transportation with a fleet of MiR robots and easy, centralized, web-based configuration. Eliminate bottlenecks and downtime with 24/7 mobile robot operation throughout your facility.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".mir-fleet-modal">More Information</button>

										<div class="modal fade mir-fleet-modal" tabindex="-1" role="dialog" aria-labelledby="mir-fleet-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">MiR Fleet</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>MiRFleet™ gives you centralized control of robots throughout your facility from a single, user-friendly, web-based interface.</p>

															<p>You can easily program and control a fleet of robots, including managing robots with different top modules, hooks or other accessories. Once programmed, the system automatically prioritizes and selects the robot best-suited for a job based on position and availability.</p>

															<p>Email notifications keep you informed of obstacles or other issues that might prevent the robot from completing a task. </p>

															<h3>Specs</h3>

															<table width="100%" style="text-align:left;">

																<tbody>

																	<tr>

																		<td width="50%"><strong>Centralised control of a fleet of robots</strong></td>

																		<td width="50%">Up to 100 robots</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Order Handling</strong></td>

																		<td width="50%">Prioritization and handling of orders among multiple robots</td>

																	</tr>	

																	<tr>

																		<td width="50%"><strong>Battery level control</strong></td>

																		<td width="50%">Monitoring of robot battery levels and automatic handling of recharging</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Traffic control</strong></td>

																		<td width="50%">Coordination of critical zones with multiple robot intersections</td>

																	</tr>

																</tbody>

															</table>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>
