<!DOCTYPE html>

<html dir="ltr" lang="en-US" prefix="og: http://ogp.me/ns#">

<head>



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="Cobots (Pty) Ltd." />

	<meta name="description" content="OnRobot provides innovative Plug & Produce End-of-Arm Tooling that help manufacturers take full advantage of the benefits of collaborative robots.">

	<meta name="robots" content="index, follow">

	

	<?php include ("favicon.php");?>


	<?php include ("stylesheets.php");?>

	<meta name="viewport" content="width=device-width, initial-scale=1" />


	<title>Brands | OnRobot | Cobots (Pty) Ltd.</title>

	<!--OPENGRAPH-->

	<meta property="og:title" content="Brands | OnRobot | Cobots (Pty) Ltd." />

	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://www.cobots.co.za/onrobot" />

	<meta property="og:image" content="https://www.cobots.co.za/assets/img/home/cobots-featured-image.png" />

		<meta property="og:image:type" content="image/png" />

		<meta property="og:image:width" content="1200" />

		<meta property="og:image:height" content="630" />

		<meta property="og:image:alt" content="Welcome to Cobots (Pty) Ltd. | Brands | OnRobot" />

</head>



<body class="stretched">



	<div id="wrapper" class="clearfix">



		<?php require("header.php"); ?>

		<script type="text/javascript" src="assets/js/lazyload.js"></script>



		<section id="brand-page-title" class="page-title-dark page-title-center" style="background-image: url('https://www.cobots.co.za/assets/img/brands/onrobot/onrobot-title-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">



			<div class="container clearfix">

				<h1 style="color:#333 !important">Introducing OnRobot</h1>

				<span style="color:#333"><strong>OnRobot provides innovative Plug & Produce End-of-Arm Tooling that help manufacturers take full advantage of the benefits of collaborative robots</strong></span>

			</div>



		</section>



		
		<section id="content">

			

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/brands/onrobot/onrobot-intro-background.png'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<div class="col_full nobottommargin center">

						<p style="text-align: justify; color: #333333;">OnRobot is a born global company with headquarters situated in the robotic cluster capital Odense, Denmark. Their vision is to be the leading collaborative End-of-Arm Tooling company and offer our customers innovative collaborative solutions with true Plug & Play functionality.</p>

						<p style="text-align: justify; color: #333333;">They provide plug-and-play electric grippers — RG2 and RG6 — that mount directly on the robot arm, are highly flexible and are simple enough to be programmed and operated from the same interface as the robot without the need of engineers. OnRobt are also developers of bio-inspired robot grippers:  a gecko-inspired gripper for handling large, flat objects, and a tactile gripper with compliant rubber tactile sensors (“skin”) to give robots a sense of touch. In addition, they also provide force/torque sensors that bring the sense of touch to industrial robots so that they can automate tasks that would otherwise require the dexterity of the human hand. </p>

					</div>	

				</div>

			</div>

			

			<div class="content-wrap"  style="background-color:#f7f7f7">

				<div class="container clearfix">

					<div class="heading-block" style="padding-top: 40px;" data-animate="fadeInLeft">

						<h1 style="color: #333">Applications</h1>

					</div>

					<div id="portfolio" class="portfolio grid-container portfolio-6 portfolio-masonry clearfix">



						<?php include("application-desc-pick-and-place.php");?>

						

						<?php include("application-desc-machine-tending.php");?>

						

						<?php include("application-desc-surface-finishing.php");?>

						<?php include("application-desc-assembly.php");?>

						<?php include("application-desc-palletising.php");?>

						<?php include("application-desc-quality-testing.php");?>

					</div>
				</div>

			</div>

			<div class="content-wrap" style="background-color:#eeeeee">

				<div class="container clearfix">

							<div class="heading-block" style="padding-top: 40px;" data-animate="fadeInLeft">

								<h1 style="color: #333">Products</h1>

							</div>

							<?php include("product-card-onrobot-rg2-gripper.php");?>

							

							

							<?php include("product-card-onrobot-rg6-gripper.php");?>

							

							

							<?php include("product-card-onrobot-dual-gripper.php");?>

							

							

							<?php include("product-card-onrobot-rg2-ft.php");?>

							

							

							<?php include("product-card-onrobot-gecko-gripper.php");?>

							

							

							<?php include("product-card-onrobot-polyskin-tactile-gripper.php");?>

							

							

							<?php include("product-card-onrobot-hex-torque-sensor.php");?>

							

							

							<?php include("product-card-onrobot-omd.php");?>

				</div>

			</div>
			<div id="related">
				<?php include ("footer-related-alternate.php")?>
			</div>


			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/brands/onrobot/onrobot-call-to-action-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<h2 style="color: #fff; text-align:center">Contact us today to add end-effectors and accessories from OnRobot to your automated production line.</h2>

						<div class="widget clearfix" style="text-align:center">

							<a href="https://www.cobots.co.za/" class="btn btn-contact">Contact Us</a>

						</div>

				</div>

			</div>

		</section>


		<?php require("footer.php"); ?>

</body>

</html>