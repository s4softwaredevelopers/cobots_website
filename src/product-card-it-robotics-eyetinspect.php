
							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="https://www.cobots.co.za/assets/img/products/accessories/it-robotics-eyetinspect.png" data-srcset="https://www.cobots.co.za/assets/img/products/accessories/it-robotics-eyetinspect.png" alt="IT Robotics eyeTinspect" align="middle" style="padding:40px 0">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>EyeT+Inspect | IT Robotics</h2>

									<p class="card-text" style="text-align:justify">EyeT+Inspect is the outstanding 3D device for robotic visual inspection combining robust 3D vision to inspect product shape with fast 2D vision to inspect product surface.</p>

									<p class="card-text" style="text-align:justify">EyeT+Inspect optimizes production thanks to automatic and objective removal of defective products from the production line.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".it-robotics-eyetinspect-modal">More Information</button>

										<div class="modal fade it-robotics-eyetinspect-modal" tabindex="-1" role="dialog" aria-labelledby="it-robotics-eyetinspect-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">EyeT+Inspect</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>EyeT+Inspect is designed to be integrated with a robot to manipulate the product in front of the device or else to manipulate the device on the product. To ease robot programming, EyeT+Inspect exploits Real-Time Visual Feedback.</p>

															<p>Real-Time Visual Feedback allows to define inspection robot path easily with real-time 3D acquisition feedback thanks to device direct connection to robot controller. New product models can be defined directly by the operator using Smart Inspect 3D, the software solution to configure the system and collect detailed statistics about the quality of the production.</p>

															<p>EyeT+Inspect can be perfectly integrated with the well-established software solution Smart Inspect 3D to configure the system and collect detailed statistics about production.</p>

															<p>Smart Inspect 3D is powered easily to use software tools for 2D and 3D visual inspection. The most flexible and easy to use software tool works by comparison: the defect is detected comparing the shape in 3D or the surface in 2D of the current product with a reference product acquired during a learning cycle.</p>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>