<!DOCTYPE html>

<html dir="ltr" lang="en-US" prefix="og: http://ogp.me/ns#">

<head>



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="Cobots (Pty) Ltd." />

	<meta name="description" content="MiR is the leading manufacturer of collaborative mobile robots offering software, accessory and robotic solutions.">

	<meta name="robots" content="index, follow">

	<?php include ("favicon.php");?>


	<?php include ("stylesheets.php");?>

	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>Brands | MiR | Cobots (Pty) Ltd.</title>

	<!--OPENGRAPH-->

	<meta property="og:title" content="Brands | MiR | Cobots (Pty) Ltd." />

	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://www.cobots.co.za/mir" />

	<meta property="og:image" content="https://www.cobots.co.za/assets/img/home/cobots-featured-image.png" />

		<meta property="og:image:type" content="image/png" />
		<meta property="og:image:width" content="1200" />

		<meta property="og:image:height" content="630" />

		<meta property="og:image:alt" content="Welcome to Cobots (Pty) Ltd. | Brands | MiR" />

</head>



<body class="stretched">

	<div id="wrapper" class="clearfix">



		<?php require("header.php"); ?>

		<script type="text/javascript" src="assets/js/lazyload.js"></script>


		<section id="brand-page-title" class="page-title-dark page-title-center" style="background-image: url('https://www.cobots.co.za/assets/img/brands/mir/mir-title-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">



			<div class="container clearfix">

				<h1 style="color:#333 !important">Introducing MiR</h1>

				<span style="color:#333"><strong>The leading manufacturer of collaborative mobile robots</strong></span>

			</div>



		</section>

		<section id="content">

			

			

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/brands/mir/mir-intro-background.png'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<div class="col_full nobottommargin center">

						<p style="text-align: justify; color: #333333;">The team at MiR are dedicated to developing user-friendly, flexible and safe robots to help companies increase the efficiency of their operations. Their autonomous robots are a new generation of advanced mobile robots which give you a rapid return on investment, often with a payback period of less than a year. These unique, collaborative robots are now used by manufacturers in a wide range of industries and healthcare sectors to automate their in-house transportation.</p>

						<p style="text-align: justify; color: #333333;">Mobile Industrial Robots is an international and ambitious company, having dedicated employees who are passionate about making a difference and creating success and innovation within the world of robotics. As a first mover in the field, MiR have enjoyed a rapid, worldwide adoption of their unique and innovative robots.</p>

						</div>	

				</div>

			</div>

			<div class="content-wrap" style="background-color:#eeeeee">

				<div class="container clearfix">

							<div class="heading-block" style="padding-top: 40px;" data-animate="fadeInLeft">

								<h1 style="color: #333">Products</h1>

							</div>

							

							<?php include("product-card-mir-mir500.php");?>

							

							<?php include("product-card-mir-mir100.php");?>

							

							<?php include("product-card-mir-mir200.php");?>

							

							<?php include("product-card-mir-mirhook100.php");?>

							

							<?php include("product-card-mir-mirhook200.php");?>

							

							<?php include("product-card-mir-pallet-lift.php");?>

							

							<?php include("product-card-mir-pallet-rack.php");?>

							

							<?php include("product-card-mir-fleet.php");?>

							

							<?php include("product-card-mir-charge.php");?>

				</div>

			</div>
			<div id="related">
				<?php include ("footer-related-alternate.php")?>
			</div>
			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/brands/mir/mir-call-to-action-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<h2 style="color: #fff; text-align:center">Contact us today to add robots, accessories and software from MiR to your automated production line.</h2>

						<div class="widget clearfix" style="text-align:center">

							<a href="https://www.cobots.co.za/contact" class="btn btn-contact">Contact Us</a>

						</div>

				</div>

			</div>

		</section>


		<?php require("footer.php"); ?>

</body>

</html>