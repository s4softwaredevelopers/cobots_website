<!DOCTYPE html>

<html dir="ltr" lang="en-US">

<head>



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="Cobots (Pty) Ltd." />

	<meta name="description" content="This is our contact page. Please drop us a message if you would like to place an order or you have any suggestions for us.">

	<meta name="robots" content="no-index, no-follow">

	<?php include ("favicon.php");?>

	<?php include ("stylesheets.php");?>

	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>Drop us a message on our contact page | Cobots (Pty) Ltd.</title>



</head>



<body class="stretched">


	<div id="wrapper" class="clearfix">



		<?php require("header.php"); ?>



		<section id="content">



			<div class="content-wrap">



				<div class="container clearfix">



					<div class="postcontent nobottommargin" style="width:100% !important;">



						<h1>Quick Contact</h1>



						<div class="contact-widget">



							<div class="contact-form-result"></div>



							<form class="nobottommargin" id="template-contactform" name="template-contactform" action="assets/include/sendemail.php" method="post">



								<div class="form-process"></div>



								<div class="col_one_third">

									<label for="template-contactform-name">Name <small>*</small></label>

									<input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required" />

								</div>



								<div class="col_one_third">

									<label for="template-contactform-email">Email <small>*</small></label>

									<input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control" />

								</div>



								<div class="col_one_third col_last">

									<label for="template-contactform-phone">Phone</label>

									<input type="text" id="template-contactform-phone" name="template-contactform-phone" value="" class="sm-form-control" />

								</div>



								<div class="clear"></div>



								<div class="col_two_third">

									<label for="template-contactform-subject">Subject <small>*</small></label>

									<input type="text" id="template-contactform-subject" name="template-contactform-subject" value="" class="required sm-form-control" />

								</div>



								<div class="col_one_third col_last">

									<label for="template-contactform-service">Services</label>

									<select id="template-contactform-service" name="template-contactform-service" class="sm-form-control">

										<option value="">-- Select One --</option>

										<option value="Sales">Sales</option>

										<option value="Consulting">Consulting</option>

										<option value="Training">Training</option>

										<option value="System Integration">System Integration</option>

										<option value="Other">Other</option>

									</select>

								</div>



								<div class="clear"></div>



								<div class="col_full">

									<label for="template-contactform-message">Message <small>*</small></label>

									<textarea class="required sm-form-control" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30"></textarea>

								</div>



								<div class="col_full hidden">

									<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />

								</div>



								

								<div class="col_full">



									<script src="https://www.google.com/recaptcha/api.js" async defer></script>

									<div class="g-recaptcha" data-sitekey="6LftM2oUAAAAAFVqEGFOgCJrntAHDoamyhqBzE27"></div>



								</div>

								

								<div class="col_full">

									<button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Send Message</button>

								</div>



							</form>



						</div>



					</div>
				</div>

			</div>

		</section>


		<?php require("footer.php"); ?>

</body>

</html>