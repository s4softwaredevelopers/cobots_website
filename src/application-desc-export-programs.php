<article class="portfolio-item">

							<div class="portfolio-image">

									<img data-toggle="modal" data-target=".export-programs-modal"  src="assets/img/icons/applications/export-programs.png" alt="Export Programs">

							</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".export-programs-modal">Export Programs</a></h3>

								<div style="text-align:center">



										<div class="modal fade export-programs-modal" tabindex="-1" role="dialog" aria-labelledby="export-programs-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Export Programs</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<h3>RoboDK Post Processors support many robot controllers, including:</h3>


														<div class="modal-list">
														<ul>

															<li>ABB RAPID (mod/prg)</li>

															<li>Fanuc LS (LS/TP)</li>

															<li>KUKA KRC/IIWA (SRC/java)</li>

															<li>Motoman Inform (JBI)</li>

															<li>Universal Robots (URP/script)</li>

															<li>and much more!</li>

														</ul>
														</div>
														</div>


													</div>

												</div>

											</div>

										</div>

								</div>

							</div>

						</article>