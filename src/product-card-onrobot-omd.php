

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/end-effectors/onrobot-omd.jpg" data-srcset="assets/img/products/end-effectors/onrobot-omd.jpg" alt="Onrobot OMD" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>OMD 3-Axis | OnRobot</h2>

									<p class="card-text" style="text-align:justify">Optoforce technology that is currently available in the OMD-10-SE-10N, OMD-20-SE-40N, OMD-20-FE-200N and OMD-30-SE-100N models</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".onrobot-omd-modal">More Information</button>

										<div class="modal fade onrobot-omd-modal" tabindex="-1" role="dialog" aria-labelledby="onrobot-omd-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">OMD 3-Axis</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body modal-list">

															<p>OnRobot OMD sensors measure the magnitude and the direction of Fx, Fy and Fz forces based purely on optical principles. We advise these sensors for low budget research programs and for measurements where torque sensing is unnecessary. Semi-spherical sensors are ideal as sensitive fingertips for humanoid robot hands, industrial grippers, harvesting robots, and due to its high durability there are various applications in the field of medical robotics (rehabilitation) and advanced robotics (e.g. exoskeletons) as well.</p>

															<p><strong>Features</strong></p>
																<ul>
																	<li>High resolution</li>

																	<li>Some of the smallest sensors in the world</li>

																	<li>Robust and durable build</li>

																	<li>Dust- and waterproof (IP65)</li>

																	<li>Quick and easy to install</li>

																	<li>Data acquisition unit included</li>

																	<li>Informative data visualisation via mini-usb</li>

																	<li>High speed - up to 1kHz</li>

																	<li>Cost-effective solution</li>

																</ul>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>

