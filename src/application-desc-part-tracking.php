<article class="portfolio-item">

							<div class="portfolio-image">

									<img data-toggle="modal" data-target=".part-tracking-modal" src="assets/img/icons/applications/part-tracking.png" alt="Part Tracking">

							</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".part-tracking-modal">Part Tracking</a></h3>

										<div class="modal fade part-tracking-modal" tabindex="-1" role="dialog" aria-labelledby="part-tracking-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Part Tracking</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>Part tracking has become simpler than ever with the advent of robotic vision systems.</p>
														</div>

													</div>

												</div>

											</div>

										</div>

							</div>

						</article>