

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/robots/mir500.png" data-srcset="assets/img/products/robots/mir500.png" alt="MiR500" align="middle" style="padding: 70px 0">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>MiR500 | MiR</h2>

									<p class="card-text" style="text-align:justify">Increase efficiency and free employees from low-value logistics tasks. MiR500™ easily automates internal transportation throughout your manufacturing or warehousing facility.</p>

									<p class="card-text" style="text-align:justify">MiR500™ is designed to automate the transportation of pallets and heavy loads across industries. With a payload of 500 kg, speed of 2 m/sec and a footprint of 1350x920 mm, MiR500™ is the largest, most powerful, fastest and robust collaborative, autonomous mobile robot from MiR.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".mir-mir500-modal">More Information</button>

										<div class="modal fade mir-mir500-modal" tabindex="-1" role="dialog" aria-labelledby="mir-mir500-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">MiR500™</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<h3>Safe and efficient maneuvering</h3>

															<p>The collaborative MIR500™ safely maneuvers around people and physical obstacles. With its advanced technology and sophisticated software, the robot autonomously navigates to find the most efficient path to its destination. When it meets obstacles, it will automatically re-route to avoid costly lags in logistics processes. MiR500™ is equipped with the latest laser-scanning technology that delivers a 360-degree visual for optimal safety. 3D cameras have a range of 30-2,000 mm above floor level to detect pallets.</p>

															<h3>Specs</h3>

															<table width="100%" style="text-align:left;">

																<tbody>

																	<tr>

																		<td width="50%"><strong>Length</strong></td>

																		<td width="50%">1350mm</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Width</strong></td>

																		<td width="50%">920mm</td>

																	</tr>	

																	<tr>

																		<td width="50%"><strong>Height</strong></td>

																		<td width="50%">320mm</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Weight (without load)</strong></td>

																		<td width="50%">250kg</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Load Surface</strong></td>

																		<td width="50%">1300mmx900mm</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Robot Payload</strong></td>

																		<td width="50%">500kg</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Certifications</strong></td>

																		<td width="50%">Complies with ISO/EN 13849 EMC requirement for light industrial use and industrial use</td>

																	</tr>

																</tbody>

															</table>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>
