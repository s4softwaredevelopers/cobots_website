<article class="portfolio-item">

							<div class="portfolio-image">

									<img data-toggle="modal" data-target=".injection-molding-modal"  src="assets/img/icons/applications/injection.png" alt="Injection Molding">

							</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".injection-molding-modal">Injection Molding</a></h3>

								<div style="text-align:center">



										<div class="modal fade injection-molding-modal" tabindex="-1" role="dialog" aria-labelledby="injection-molding-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Injection Molding</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>A collaborative robot for injection molding allows for rapid and accurate handling of injection molds used for prototyping and short-run manufacturing. Having a collaborative robot enables relief for machine operators from physically demanding, repetitive work and eliminate the risk of injuries.</p> 

															<p>The collaborative robot is able to run most applications autonomously, allowing your business to handle molding even when employees are not there after business hours.</p> 

															<p>For added connectivity and flexibility, Euromap 67 can be added to your collaborative robot. Euromap 67 consists of a cable connection to the injection molding machine, user friendly templates which ensure easy and fast programming as well as communication signals between the robot arm and the injection molding.</p> 
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>

							</div>

						</article>