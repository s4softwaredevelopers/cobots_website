							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/accessories/robotiq-force-sensor.png" data-srcset="assets/img/products/accessories/robotiq-force-sensor.png" alt="Robotiq Force Sensor" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>FT 300 Force Torque Sensor | Robotiq</h2>

									<p class="card-text" style="text-align:justify">The FT 300 enables force sensitive applications with Universal Robots. Applications include machine tending,finishing, insertion, quality testing, assembly and pick & place.</p>

									<p class="card-text" style="text-align:justify">The FT 300 is built for compatability with Universal Robots, it's simple to install and takes precise, repeatable and high-resolution measurements. The stiff metal compostition ensures high accuracy.</p>

								</div>

							</div>