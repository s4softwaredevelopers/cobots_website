

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/software/robodk-software-pack.png" data-srcset="assets/img/products/software/robodk-software-pack.png" alt="RoboDK Software" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>RoboDK</h2>

									<p class="card-text" style="text-align:justify">The software from RoboDK allows you to program any Industrial Robot offline with one simulation environment. With this software you are able to convert NC programs to robot programs, generate robot programs offline, export programs to your robot, callibrate your robot arm for improved accuracy, as well as create virtual environments to simulate robot applications. In addition, an extensive robot library is made available to you.</p>

								</div>

							</div>

