<article class="portfolio-item">

							<div class="portfolio-image">

									<img data-toggle="modal" data-target=".bin-picking-modal" src="assets/img/icons/applications/bin-picking.png" alt="Bin Picking">

							</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".bin-picking-modal">Bin Picking</a></h3>

								<div style="text-align:center">



										<div class="modal fade bin-picking-modal" tabindex="-1" role="dialog" aria-labelledby="bin-picking-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Bin Picking</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>Bin-picking consists of three main types: structured, semi-structured and random. In this day and age, it is becoming more and more viable to utilise robots for random bin-picking with the advanced vision systems and programs being created. Remove the effort from unloading, sorting and packing by combining one of the UR robots with a vision system.</p>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>

							</div>

						</article>