

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/software/robotiq-skills.png" data-srcset="assets/img/products/software/robotiq-skills.png" alt="Robotiq Skills" align="middle" style="padding:50px 0">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>Skills | Robotiq</h2>

									<p class="card-text" style="text-align:justify">Skills are to robots what apps are to phones. They accelerate robot programming by providing ready-to-use, downloadable robotic programs. They extend the built-in capabilities of the robot with a broader variety of specialized tasks. </p>

									<p class="card-text" style="text-align:justify">Every Skills package includes a complete program to download on a USB key, a step-by-step procedure describing how to use the application itself, and a demonstration video. Skills are free for all users of Robotiq Plug & Play components or Insights subscribers.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".robotiq-skills-modal">More Information</button>

										<div class="modal fade robotiq-skills-modal" tabindex="-1" role="dialog" aria-labelledby="robotiq-skills-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Skills</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>Automation engineers can download and reuse skills instead of teaching or programming a complex robotic task that has already been solved.</p>

															<p>Once the Skill is installed, activate it by inserting a Script node and call the Skill from the functions menu. Your robot is now able to do more, increasing precision, repeatability and overall productivity.</p>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>
