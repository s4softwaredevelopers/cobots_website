

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/software/robotiq-insights.png" data-srcset="assets/img/products/software/robotiq-insights.png" alt="Robotiq Insights" align="middle" style="padding:50px 0">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>Insights | Robotiq</h2>

									<p class="card-text" style="text-align:justify">Insights sends you a text message when your robot requires your attention, and provides real-time data to monitor, troubleshoot and improve your production. It is currently available in the United States, Canada and Europe.</p>

									<p class="card-text" style="text-align:justify">Monitor every Universal Robots application with Insights. Get the right KPIs for your specific process to improve your cell productivity and product quality. Receive real-time alerts and access all the data to diagnose failures and get your cell back in production faster. Analyze trends in robot performance to improve overall productivity.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".robotiq-insights-modal">More Information</button>

										<div class="modal fade robotiq-insights-modal" tabindex="-1" role="dialog" aria-labelledby="robotiq-insights-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Insights</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>Insights is fast and easy to install, with step-by-step support to help you monitor your Universal Robots production within minutes. Create your custom alerts to get a notification for any stop in production.</p>

															<p>Insights gives a real-time view of the robot’s key performance indicators (KPIs) in operation. You’ll assess cycles completed, efficiency, wait-time, and overall utilization for each robot.</p>

															<p><strong>Easy to integrate and easy to use, features include:</strong></p>
																<div class="modal-list">
																<ul>

																	<li>Monitor your robots in real time, from anywhere</li>

																	<li>Define your own KPIs and track data that matters</li>

																	<li>Measure, document and report on production progress </li>

																	<li>Troubleshoot your robotic cell </li>

																	<li>Analyze and improve your robot ROI</li>

																	<li>Get SMS Alerts on exceptions or when the robot requires attention</li>

																</ul>
																</div>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>