<article class="portfolio-item">

							<div class="portfolio-image">

									<img data-toggle="modal" data-target=".pick-and-place-modal"  src="assets/img/icons/applications/pick-and-place.png" alt="Pick and Place">

							</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".pick-and-place-modal">Pick and Place</a></h3>

								<div style="text-align:center">



										<div class="modal fade pick-and-place-modal" tabindex="-1" role="dialog" aria-labelledby="pick-and-place-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Pick and Place</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>A collaborative robot can add value to your production by automating repetitive tasks with a collaborative tailored for pick and place. A collaborative robot can allow for increased accuracy and reduced waste. A collaborative robot arm such as a UR5 can be used in repetitive pick and place tasks offering flexibility such as releiving your workforce for more important tasks.</p>

															<p>A UR robot can run and pick and place applications autonomously, allowing your business to handle inventory after your employees have gone home. A collaborative robot such as the UR arm can easily be moved/deployed/adjusted into a new process; giving you the aglity to automate almost any manual task, including small batches or fast change-overs.</p> 

															<p>By adding an accessory such as an OnRobot RG2 Gripper or the RG6 Gripper, you will increase production output by accelerating your pick and place processes.</p>
															<p>A Robotiq solution for pick and place application would be a Robotiq Wrist camera and a 2 Finger Gripper on a UR5. This allows for production to run non-stop when delays are tight.</p>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>

							</div>

						</article>