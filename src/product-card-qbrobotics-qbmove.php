

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/accessories/qbrobotics-qbmove.png" data-srcset="assets/img/products/accessories/qbrobotics-qbmove.png" alt="qbRobotics qbMove" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>qbMove | qbRobotics</h2>

									<p class="card-text" style="text-align:justify">The qbMove is a modular VSA (Variable Stiffness Actuator), an innovative muscle-like building block for the realization of soft robots.</p>

									<p class="card-text" style="text-align:justify">The VSA technology enables to regulate the stiffness of the motor endowing robots with Natural Motion.</p>

								</div>



							</div>
