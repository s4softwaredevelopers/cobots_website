
							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="https://www.cobots.co.za/assets/img/products/accessories/easyrobotics-profeeder.png" data-srcset="https://www.cobots.co.za/assets/img/products/accessories/easyrobotics-profeeder.png" alt="Easy+ Software" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>Profeeder | EasyRobotics</h2>

									<p class="card-text" style="text-align:justify">ProFeeder forms part of Stage 2 expansion. The ProFeeder enables you to increase efficiency in order to run small and medium series production in an automated two-shift operation on CNC or milling machine. This solution is mobile, compact and easy to move around between machines with a pallet CNC.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".easyrobotics-profeeder-modal">More Information</button>

										<div class="modal fade easyrobotics-profeeder-modal" tabindex="-1" role="dialog" aria-labelledby="easyrobotics-profeeder-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Profeeder</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body modal-list">
														<ul>	
															<li>ROI: < 1 year</li>

															<li>Operation of parts tray:  Changing the mover</li>

															<li>Mobility: Easily removable</li>

															<li>Series size: Small series</li>
															
															<li>Number of part trays: 2</li>	
															<li>Movers: 2</li>
															<li>Production: 2-shift operation</li>
															<li>Dimensions: 950x860x1125mm</li>

														</ul>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>