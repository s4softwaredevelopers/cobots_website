<!DOCTYPE html>

<html dir="ltr" lang="en-US" prefix="og: http://ogp.me/ns#">

<head>



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="Cobots (Pty) Ltd." />

	<meta name="description" content="EasyRobotics is a Denmark based robotics company with over 40 years of experience in designing, developing and producing advanced machines for industrial applications.">

	<meta name="robots" content="index, follow">

	<?php include ("favicon.php");?>

	<?php include ("stylesheets.php");?>

	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>Brands | EasyRobotics | Cobots (Pty) Ltd.</title>

	<!--OPENGRAPH-->

	<meta property="og:title" content="Brands | EasyRobotics | Cobots (Pty) Ltd." />

	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://www.cobots.co.za/easyrobotics" />

	<meta property="og:image" content="https://www.cobots.co.za/assets/img/home/cobots-featured-image.png" />

		<meta property="og:image:type" content="image/png" />

		<meta property="og:image:width" content="1200" />

		<meta property="og:image:height" content="630" />

		<meta property="og:image:alt" content="Welcome to Cobots (Pty) Ltd. | Brands | EasyRobotics" />

</head>



<body class="stretched">


		<div id="wrapper" class="clearfix">



		<?php require("header.php"); ?>

		<script type="text/javascript" src="assets/js/lazyload.js"></script>



		<section id="brand-page-title" class="page-title-dark page-title-center" style="background-image: url('https://www.cobots.co.za/assets/img/brands/easyrobotics/easyrobotics-title-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">



			<div class="container clearfix">

				<h1 style="color:#333 !important">Introducing EasyRobotics</h1>

				<span style="color:#333"><strong>With over 40 years of experience in designing, developing and producing advanced machines for industrial applications</strong></span>

			</div>



		</section>

		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

						<div class="col_full nobottommargin center">

						<p style="text-align: justify; color: #333333;">They provide ProFeeders which are a mobile and compact automated cell that can be mounted on a robot which can increase automation within the production process. This makes it much easier for parts to be fed in and out of fully automatic CNC lathes and milling machines. EasyRobotics also developed the ER5 Cobot platform which is a compact mobile workstation for safe collaboration with robot arms. The ER5 can be used in fully or partially automated production environments which demand flexibility and rapid integration.</p>
						</div>	

				</div>

			</div>

			



			<div class="content-wrap"  style="background-color:#f7f7f7">

				<div class="container clearfix">

					<div class="heading-block" style="padding-top: 40px;" data-animate="fadeInLeft">

						<h1 style="color: #333">Applications</h1>

					</div>

					<div id="portfolio" class="portfolio grid-container portfolio-6 portfolio-masonry clearfix">

						<?php include("application-desc-assembly.php");?>

						<?php include("application-desc-pick-and-place.php");?>

						<?php include("application-desc-cnc.php");?>

						<?php include("application-desc-machine-tending.php");?>

						<?php include("application-desc-gluing.php");?>

						<?php include("application-desc-infinite-possibilities.php");?>

					</div>

				</div>

			</div>

			<div class="content-wrap" style="background-color:#eeeeee">

				<div class="container clearfix">

							<div class="heading-block" style="padding-top: 40px;" data-animate="fadeInLeft">

								<h1 style="color: #333">Products</h1>

							</div>

							<?php include("product-card-easyrobotics-er5-mobile-cobot-platform.php");?>

							<?php include("product-card-easyrobotics-profeeder-light.php");?>

							<?php include("product-card-easyrobotics-profeeder.php");?>

							<?php include("product-card-easyrobotics-profeeder-multi.php");?>

							<?php include("product-card-easyrobotics-easy-plus.php");?>


				</div>

			</div>
			<div id="related">
				<?php include ("footer-related.php")?>
			</div>

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/brands/easyrobotics/easyrobotics-call-to-action-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<h2 style="color: #fff; text-align:center">Contact us today to integrate these fantastic options from EasyRobotics</h2>

						<div class="widget clearfix" style="text-align:center">

							<a href="https://www.cobots.co.za/contact" class="btn btn-contact">Contact Us</a>

						</div>

				</div>

			</div>

		</section>


		<?php require("footer.php"); ?>

</body>

</html>