

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/robots/mir-pallet-rack.png" data-srcset="assets/img/products/robots/mir-pallet-rack.png" alt="MiR Pallet Rack" align="middle" style="padding:40px 0">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>MiR500 Pallet Rack | MiR</h2>

									<p class="card-text" style="text-align:justify">The MiR500™  Pallet Rack is the delivery station for your EUR-pallets. The MIR500™  drives autonomously into the MiR500™  Pallet Rack and delivers or pick up pallets safely. MiR500™  Pallet Rack is the delivery station for your EUR-pallets. It enables the MiR500™  to autonomously collect and deliver pallets with the MiR500™  Pallet lift, for stable transportation of payloads up to 500 kg.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".mir-pallet-rack-modal">More Information</button>

										<div class="modal fade mir-pallet-rack-modal" tabindex="-1" role="dialog" aria-labelledby="mir-pallet-rack-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">MiR500™  Pallet Rack</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>When equipped with MiR500™  Pallet Lift, the MiR500 can drive into the delivery station MiR500™  Pallet Rack to pickup or deliver EUR-pallets safely, efficiently and in a completely autonomous way, freeing up employees for more valuable tasks. MiR500™  has a speed of 2 m/sec (7,2 km/hour) for extremely efficient transportation time. There is no need to change the facility layout when operating the MiR500™  as the function of wires, magnets or QR codes is replaced by sophisticated navigation software. Simply download CAD files of the facility to the robot or use its laser scanners to create a map - that’s all it takes to ensure a fast and cost-efficient implementation.</p>

															<h3>Specs</h3>

															<table width="100%" style="text-align:left;">

																<tbody>

																	<tr>

																		<td width="50%"><strong>Length</strong></td>

																		<td width="50%">1430mm</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Width</strong></td>

																		<td width="50%">1142mm</td>

																	</tr>	

																	<tr>

																		<td width="50%"><strong>Height when lowered</strong></td>

																		<td width="50%"> 357mm</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Pallet Rack Payload</strong></td>

																		<td width="50%">500kg</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>EUR-Pallets Dimensions</strong></td>

																		<td width="50%">1200mmx800mm</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Pallet Production Specs</strong></td>

																		<td width="50%">EN 13698-1</td>

																	</tr>

																</tbody>

															</table>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>
