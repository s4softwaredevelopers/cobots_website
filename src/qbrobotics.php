<!DOCTYPE html>

<html dir="ltr" lang="en-US" prefix="og: http://ogp.me/ns#">

<head>



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="Cobots (Pty) Ltd." />

	<meta name="description" content="qbRobotics fosters innovation, driving the diffusion of technologies that will propel the next generation of robots.">

	<meta name="robots" content="index, follow">

	<?php include ("favicon.php");?>

	<?php include ("stylesheets.php");?>

	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>Brands | qbRobotics | Cobots (Pty) Ltd.</title>

	<!--OPENGRAPH-->

	<meta property="og:title" content="Brands | qbRobotics | Cobots (Pty) Ltd." />

	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://www.cobots.co.za/qbrobotics" />

	<meta property="og:image" content="https://www.cobots.co.za/assets/img/home/cobots-featured-image.png" />

		<meta property="og:image:type" content="image/png" />

		<meta property="og:image:width" content="1200" />

		<meta property="og:image:height" content="630" />

		<meta property="og:image:alt" content="Welcome to Cobots (Pty) Ltd. | Brands | qbRobotics" />

</head>



<body class="stretched">


		<div id="wrapper" class="clearfix">



		<?php require("header.php"); ?>

		<script type="text/javascript" src="assets/js/lazyload.js"></script>



		<section id="brand-page-title" class="page-title-dark page-title-center" style="background-image: url('https://www.cobots.co.za/assets/img/brands/qbrobotics/qbrobotics-title-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">



			<div class="container clearfix">

				<h1 style="color:#333 !important">Introducing qbrobotics</h1>

				<span style="color:#333"><strong>Fostering innovation and driving the diffusion of technologies that will propel the next generation of robots</strong></span>

			</div>



		</section>

		<section id="content">

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/brands/qbrobotics/qbrobotics-intro-background.png'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<div class="col_full nobottommargin center">

						<p style="text-align: justify; color: #333333;">qbrobotics is an Italian Company founded in 2011 as a spin-off of the University of Pisa – Research Center “E. Piaggio” and the Italian Institute of Technology that produce innovative devices implementing the soft-robotics technology as robotic hands, grippers, handles, delta robots and VSA actuators.</p>

						<p style="text-align: justify; color: #333333;">These products implement the natural principles of motion control, the same principle behind the muscular system of animals and the natural human movement, in order to create robots that are smooth and safe when interacting with humans. The industrial sectors which qbrobotics operates in are service robotics, industrial robotics & automation, as well as biomedical and prosthetics.</p>
						</div>	

				</div>

			</div>

			



			<div class="content-wrap"  style="background-color:#f7f7f7">

				<div class="container clearfix">

					<div class="heading-block" style="padding-top: 40px;" data-animate="fadeInLeft">

						<h1 style="color: #333">Applications</h1>

					</div>

					<div id="portfolio" class="portfolio grid-container portfolio-6 portfolio-masonry clearfix">

						<?php include("application-desc-pick-and-place.php");?>

						<?php include("application-desc-injection-molding.php");?>

						<?php include("application-desc-lab-testing.php");?>

						<?php include("application-desc-assembly.php");?>

						<?php include("application-desc-bin-picking.php");?>

						<?php include("application-desc-infinite-possibilities.php");?>

					</div>

				</div>

			</div>

			<div class="content-wrap" style="background-color:#eeeeee">

				<div class="container clearfix">

							<div class="heading-block" style="padding-top: 40px;" data-animate="fadeInLeft">

								<h1 style="color: #333">Products</h1>

							</div>

							<?php include("product-card-qbrobotics-qbmove.php");?>

							<?php include("product-card-qbrobotics-qbmove-advanced.php");?>

							<?php include("product-card-qbrobotics-qbsofthand-research.php");?>

							<?php include("product-card-qbrobotics-flat-flange-addon.php");?>

							<?php include("product-card-qbrobotics-flat-flange.php");?>

							<?php include("product-card-qbrobotics-c-flange.php");?>

				</div>

			</div>
			<div id="related">
				<?php include ("footer-related.php")?>
			</div>

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/brands/qbrobotics/qbrobotics-call-to-action-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<h2 style="color: #fff; text-align:center">Contact us today to add robots, end-effectors and accessories from qbRobotics to your automated production line.</h2>

						<div class="widget clearfix" style="text-align:center">

							<a href="https://www.cobots.co.za/contact" class="btn btn-contact">Contact Us</a>

						</div>

				</div>

			</div>

		</section>


		<?php require("footer.php"); ?>

</body>

</html>