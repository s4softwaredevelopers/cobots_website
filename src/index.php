<!DOCTYPE html>

<html dir="ltr" lang="en-US">

<head>



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="Cobots (Pty) Ltd." />

	<meta name="description" content="Welcome to the home of Cobots (Pty) Ltd., we are the leaders in Human-Robot manufacturing collaboration solutions in South Africa.">

	<meta name="robots" content="index, follow">

	<?php include ("favicon.php");?>


	<?php include ("stylesheets.php");?>

	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>Welcome to Cobots (Pty) Ltd. Home | Collaborative Robotics Specialists</title>
	
	<!--OPENGRAPH-->

	<meta property="og:title" content="Welcome to Cobots (Pty) Ltd. Home | Collaborative Robotics Specialists" />

	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://www.cobots.co.za" />

	<meta property="og:image" content="https://www.cobots.co.za/assets/img/home/cobots-featured-image.png" />

		<meta property="og:image:type" content="image/png" />

		<meta property="og:image:width" content="1200" />

		<meta property="og:image:height" content="630" />

		<meta property="og:image:alt" content="Welcome to Cobots (Pty) Ltd. Home | Collaborative Robotics Specialists" />

	<meta name="google-site-verification" content="ueoOTa6dY2gYKR3-QF8G_wmxPO8WimKoYKfUdl4i72Y" />

</head>



<body class="stretched">

	<div id="wrapper" class="clearfix">



		<?php require("header.php"); ?>

		<section id="page-title" class="page-title-dark page-title-center" style="background-image: url('https://www.cobots.co.za/assets/img/misc/cobots-home-title-background.jpg'); background-size: cover;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">



			<div class="container clearfix">

				<h1>Introducing Cobots</h1>

				<span><strong>Enhancing manufacturing through collaborative robots</strong></span>

			</div>



		</section>
		

		<section id="content">

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/misc/cobots-home-intro-background.png'); background-size: cover;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<div class="col_full nobottommargin center">

							<div class="heading-block" style="padding-top: 40px;" data-animate="fadeInLeft">

								<h2 style="color: #333">Welcome to the world of collaborative robots</h2>

							</div>

						<p style="text-align: justify; color: #333333;">Cobots (PTY) Ltd motivation and aspiration is to bring new cutting edge collaborative solutions to the market that will be easily deployable, helps increase productivity, reduce injury, improve quality and boost morale.</p>

						<p style="text-align: justify; color: #333333;">Our mission is to bring collaborative robotic technologies that can be beneficial to all aspects of task-based businesses - no matter what the companies size.  We are committed to becoming advanced in the fields of coolaborative robots and technology by providing, engaging and promoting products that will be flexible, plug & play collaborative robot tools.</p>
						<p style="text-align:justify; color: #333333;">At Cobots we strive to make it simple and affordable for humans and machines to work together in order to improve workflow and production. Cobots offers a variety of different collaborative robots as well as accessories that are easy to integrate into many production lines across various industries. As a registered distributor in South Africa, Cobots will walk you through the process of integrating collaborative robots into your current manufacturing process. We are partnered with 8 international brands, all of which specialize in different facets of the robotic world, ranging from software programming to industrial robot covers.</p>
						</div>	

				</div>

			</div>

			<div class="content-wrap"  style="background-color:#f7f7f7">

				<div class="container clearfix">

					<div class="heading-block" style="padding-top: 40px;" data-animate="fadeInLeft">

						<h2 style="color: #333">Our collaborative robots allow for numerous applications including, but not limited to:</h2>

					</div>

					<div id="portfolio" class="portfolio grid-container portfolio-6 portfolio-masonry clearfix">
						<?php include("application-desc-pick-and-place.php");?>
						<?php include("application-desc-vision-inspection.php");?>
						<?php include("application-desc-lab-testing.php");?>
						<?php include("application-desc-export-programs.php");?>
						<?php include("application-desc-assembly.php");?>
						<article id="application6" class="portfolio-item">
							<div class="portfolio-image">
									<img src="https://www.cobots.co.za/assets/img/icons/applications/infinite-possibilities.png" alt="Infinite Possibilities">
							</div>
							<div class="portfolio-desc">
								<h3><a href="https://www.cobots.co.za/applications">View More</a></h3>
							</div>
						</article>
					</div>
				</div>

			</div>

			

			

			<div class="content-wrap" style="background-color: #fff;padding-bottom: 0px;">

				<div class="container clearfix">

					<div class="heading-block" style="padding-top: 40px;" data-animate="fadeInLeft">

						<h2 style="color: #333">Brands we Endorse</h2>

					</div>

				</div>
				<div id="related-portfolio" class="owl-carousel owl-carousel-full portfolio-carousel portfolio-notitle portfolio-nomargin footer-stick carousel-widget" data-margin="0" data-nav="true" data-pagi="false" data-items-xs="1" data-items-sm="2" data-items-md="3" data-items-lg="4" style="margin-bottom: 0 !important;">



					

					<div class="oc-item">

						<div class="iportfolio">

							<div class="portfolio-image">

								<a href="https://www.cobots.co.za/universal-robots">

									<img src="https://www.cobots.co.za/assets/img/brands/universal-robots/universal-robots-brand-card.jpg" alt="Universal Robots">

								</a>

								<div class="portfolio-overlay">

									<a href="https://www.cobots.co.za/universal-robots" class="right-icon"><i class="icon-line-ellipsis"></i></a>

								</div>

							</div>

							<div class="portfolio-desc">

								<h3><a href="https://www.cobots.co.za/universal-robots">Universal Robots</a></h3>

								<p>Suppliers of Robots</p>

							</div>

						</div>

					</div>

					<div class="oc-item">

						<div class="iportfolio">

							<div class="portfolio-image">

								<a href="https://www.cobots.co.za/mir">

									<img src="https://www.cobots.co.za/assets/img/brands/mir/mir-brand-card.jpg" alt="MiR">

								</a>

								<div class="portfolio-overlay">

									<a href="https://www.cobots.co.za/mir" class="right-icon"><i class="icon-line-ellipsis"></i></a>

								</div>

							</div>

							<div class="portfolio-desc">

								<h3><a href="https://www.cobots.co.za/mir">MiR</a></h3>

								<p>Suppliers of Robots | Accessories | Software</p>

							</div>

						</div>

					</div>

					<div class="oc-item">

						<div class="iportfolio">

							<div class="portfolio-image">

								<a href="https://www.cobots.co.za/robotiq">

									<img src="https://www.cobots.co.za/assets/img/brands/robotiq/robotiq-brand-card.jpg" alt="Robotiq">

								</a>

								<div class="portfolio-overlay">

									<a href="https://www.cobots.co.za/robotiq" class="right-icon"><i class="icon-line-ellipsis"></i></a>

								</div>

							</div>

							<div class="portfolio-desc">

								<h3><a href="https://www.cobots.co.za/robotiq">Robotiq</a></h3>

								<p>Suppliers of End-Effectors | Accessories | Software</p>

							</div>

						</div>

					</div>

					<div class="oc-item">

						<div class="iportfolio">

							<div class="portfolio-image">

								<a href="https://www.cobots.co.za/it-robotics">

									<img src="https://www.cobots.co.za/assets/img/brands/it-robotics/it-robotics-brand-card.jpg" alt="IT Robotics">

								</a>

								<div class="portfolio-overlay">

									<a href="https://www.cobots.co.za/it-robotics" class="right-icon"><i class="icon-line-ellipsis"></i></a>

								</div>

							</div>

							<div class="portfolio-desc">

								<h3><a href="https://www.cobots.co.za/it-robotics">IT Robotics</a></h3>

								<p>Suppliers of Accessories | Software</p>

							</div>

						</div>

					</div>

					<div class="oc-item">

						<div class="iportfolio">

							<div class="portfolio-image">

								<a href="https://www.cobots.co.za/onrobot">

									<img src="https://www.cobots.co.za/assets/img/brands/onrobot/onrobot-brand-card.jpg" alt="OnRobot">

								</a>

								<div class="portfolio-overlay">

									<a href="https://www.cobots.co.za/onrobot" class="right-icon"><i class="icon-line-ellipsis"></i></a>

								</div>

							</div>

							<div class="portfolio-desc">

								<h3><a href="https://www.cobots.co.za/onrobot">OnRobot</a></h3>

								<p>Suppliers of End-Effectors | Accessories</p>

							</div>

						</div>

					</div>

					<div class="oc-item">

						<div class="iportfolio">

							<div class="portfolio-image">

								<a href="https://www.cobots.co.za/qbrobotics">

									<img src="https://www.cobots.co.za/assets/img/brands/qbrobotics/qbrobotics-brand-card.jpg" alt="qbRobotics">

								</a>

								<div class="portfolio-overlay">

									<a href="https://www.cobots.co.za/qbrobotics" class="right-icon"><i class="icon-line-ellipsis"></i></a>

								</div>

							</div>

							<div class="portfolio-desc">

								<h3><a href="https://www.cobots.co.za/qbrobotics">qbRobotics</a></h3>

								<p>Suppliers of Robots | End-Effectors | Accessories</p>

							</div>

						</div>

					</div>

					<div class="oc-item">

						<div class="iportfolio">

							<div class="portfolio-image">

								<a href="https://www.cobots.co.za/robodk">

									<img src="https://www.cobots.co.za/assets/img/brands/robodk/robodk-brand-card.jpg" alt="RoboDK">

								</a>

								<div class="portfolio-overlay">

									<a href="https://www.cobots.co.za/robodk" class="right-icon"><i class="icon-line-ellipsis"></i></a>

								</div>

							</div>

							<div class="portfolio-desc">

								<h3><a href="https://www.cobots.co.za/robodk">Robo DK</a></h3>

								<p>Suppliers of Software</p>

							</div>

						</div>

					</div>

					<div class="oc-item">

						<div class="iportfolio">

							<div class="portfolio-image">

								<a href="https://www.cobots.co.za/robo-gear">

									<img src="https://www.cobots.co.za/assets/img/brands/robo-gear/robo-gear-brand-card.jpg" alt="Robo-Gear">

								</a>

								<div class="portfolio-overlay">

									<a href="https://www.cobots.co.za/robo-gear" class="right-icon"><i class="icon-line-ellipsis"></i></a>

								</div>

							</div>

							<div class="portfolio-desc">

								<h3><a href="https://www.cobots.co.za/robo-gear">Robo-Gear</a></h3>

								<p>Suppliers of Accessories</p>

							</div>

						</div>

					</div>

					<div class="oc-item">

						<div class="iportfolio">

							<div class="portfolio-image">

								<a href="https://www.cobots.co.za/brands">

									<img src="https://www.cobots.co.za/assets/img/brands/all-brands-brand-card.jpg" alt="View All Brands">

								</a>

								<div class="portfolio-overlay">

									<a href="https://www.cobots.co.za/brands" class="right-icon"><i class="icon-line-ellipsis"></i></a>

								</div>

							</div>

							<div class="portfolio-desc">

								<h3><a href="https://www.cobots.co.za/brands">View all Brands</a></h3>

								<p>What are you waiting for? Check out our brands!</p>

							</div>

						</div>

					</div>

				</div>

			</div>



			<div class="content-wrap" style="background-color:#f7f7f7; padding: 0px !important;">

				<div style="background-image: url('https://www.cobots.co.za/assets/img/brands/universal-robots/ur-intro-background.png'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

					<div class="container clearfix">

						<div class="col_full nobottommargin center">

							<div class="heading-block" style="padding-top: 40px;" data-animate="fadeInLeft">

								<h2 style="color: #333">Collaborative Robots, Your Industry & Industry 4.0</h2>

								<h3 style="color: #333">How does it all fit?

							</div>

							<p style="text-align: justify; color: #333333;">Industry 4.0, more commonly referred to as the fourth industrial revolution, is revolutionising the manner in which traditional factories are run. It has given birth to what are known as "smart factories" by including cyber-physical systems, cloud and cognitive computing. The cognitive technologies used within these "smart factories" allow the extraction of relevant information together in real-time and apply analytics to harvest unparalleled levels of understanding and insights about their manufacturing process.</p>

							<p style="text-align: justify; color: #333333;">Automation of production is a key facet in today's fast-changing production environments. Collaborative robots can be adjusted and programmed to add value to any environment by taking over repetitive and high-precision tasks. These robots continue to evolve through software updates and their own programmability, bringing unparalleled returns on investment.</p>
							
							<p style="text-align: center;"><a href="https://www.cobots.co.za/industry-4" class="btn btn-primary">Read More</a></p>
					
						</div>	

					</div>

				</div>

			</div>

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/misc/cobots-home-call-to-action-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<h2 style="color: #fff; text-align:center; text-transform:uppercase;">Contact us today to assist you with your automation needs</h2>

						<p style="text-align:center; color:#ffffff"><strong>Cobots is your one-stop shop for all your automation needs. Contact us today!</strong></p>

						<div class="widget clearfix" style="text-align:center">

							<a href="https://www.cobots.co.za/contact" class="btn btn-contact">Contact Us</a>

						</div>

				</div>

			</div>

			



			

			

			

		</section>
		

		

		<?php require("footer.php"); ?>

</body>

</html>