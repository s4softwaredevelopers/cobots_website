<article class="portfolio-item">

							<div class="portfolio-image">

									<img data-toggle="modal" data-target=".motion-detection-modal"  src="assets/img/icons/applications/motion-detection.png" alt="Motion Detection">

							</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".motion-detection-modal">Motion Detection</a></h3>

								<div style="text-align:center">



										<div class="modal fade motion-detection-modal" tabindex="-1" role="dialog" aria-labelledby="motion-detection-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Motion Detection</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>With the advancement in motion detecting sensors, it has become even safer to incorporate collaborative robots into your current factory setup. The collaborative robots are able to safely and autonomously manoeuvre around human workers to accomplish their tasks with the addition of these sensors.</p>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>

							</div>

						</article>