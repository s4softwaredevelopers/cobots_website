
		<header id="header" class="dark">



			<div id="header-wrap">



				<div class="container clearfix">



					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>


					<div id="logo">

						<a href="https://www.cobots.co.za" class="standard-logo" data-dark-logo="https://www.cobots.co.za/assets/img/logo/rectangular-logo-dark.png" data-sticky-logo="https://www.cobots.co.za/assets/img/logo/square-logo-dark.png" data-mobile-logo="https://www.cobots.co.za/assets/img/logo/square-logo-dark.png"><img src="https://www.cobots.co.za/assets/img/logo/rectangular-logo-dark.png" alt="Cobots (Pty) Ltd. Logo"></a>

						<a href="https://www.cobots.co.za" class="retina-logo" data-dark-logo="https://www.cobots.co.za/assets/img/logo/rectangular-logo-retina-dark.png" data-sticky-logo="https://www.cobots.co.za/assets/img/logo/square-logo-retina-dark.png" data-mobile-logo="https://www.cobots.co.za/assets/img/logo/square-logo-retina-dark.png"><img src="https://www.cobots.co.za/assets/img/logo/rectangular-logo-retina-dark.png" alt="Cobots (Pty) Ltd. Logo"></a>

					</div>


					
					<nav id="primary-menu">



						<ul>

							<li><a href="https://www.cobots.co.za"><div>Home</div></a></li>

							<li><a href="https://www.cobots.co.za/products"><div>Products</div></a>

								<ul>

									<li><a href="https://www.cobots.co.za/robots"><div>Robots</div></a></li>

									<li><a href="https://www.cobots.co.za/end-effectors"><div>End-Effectors</div></a></li>

									<li><a href="https://www.cobots.co.za/accessories"><div>Accessories</div></a></li>

									<li><a href="https://www.cobots.co.za/software"><div>Software</div></a></li>

								</ul>

							</li>

							<li><a href="https://www.cobots.co.za/brands"><div>Brands</div></a></li>

							<li><a href="https://www.cobots.co.za/applications"><div>Applications</div></a></li>

							<li><a href="https://www.cobots.co.za/industries"><div>Industries</div></a></li>

							<li><a href="https://www.cobots.co.za/contact" class="btn-contact"><div>Contact Us</div></a></li>

						</ul>



					</nav>


				</div>



			</div>



		</header>