<article class="portfolio-item">
	<div class="portfolio-image">
		<img data-toggle="modal" data-target=".assembly-modal" src="assets/img/icons/applications/assembly.png" alt="Assembly">
	</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".assembly-modal">Assembly</a></h3>

										<div class="modal fade assembly-modal" tabindex="-1" role="dialog" aria-labelledby="assembly-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Assembly</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>A collaborative robot arm within an assembly line allows for reduced assembly lines, an increase in production speed and improve the quality of the product. A collaborative robot can easily be redeployed which is a benefit for small and medium sized businesses which has a high mix/low volume production.</p>
															<p>Assembly operations tend to require a robot to locate and grip precisely. This can be achieved through a full collaborative set-up. A full collaborative set-up comprises of:</p>
															<div style="padding: 0 100px">
															<ul>
																<li>UR5 OR UR10</li>
																<li>OnRobot RG2 Gripper</li>
																<li>OnRobot RG-2 FT</li>
																<li>OnRobot Torque Sensor</li>
															</ul>
															</div>

															<p>The advantage of having a full collaborative set-up allows the collaborative robot to work side by side with your workers. Another advantage that stems from a collaborative robot is that your workers can be relieved of other tasks having the collabaroative robot take over the repetitive tasks. UR robots can be programmed to operate in the reduced mode when a human enters the robots work area and resume full speed when the person leaves the area. A collaborative robot can replace human operators in dirty, dangerous tasks to reduce repetitive strain and accidental injury.</p>

															<p>Universal Robots can handle various materials such as assembly plastics, woods and metals.</p>

															<p>By implementing a collaborative robot in your assembly line it allows you to expand production capabilities such as increasing quality, consistency and production speed.</p>
														</div>

													</div>

												</div>

											</div>

										</div>

							</div>

						</article>