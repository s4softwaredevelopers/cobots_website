<!DOCTYPE html>

<html dir="ltr" lang="en-US" prefix="og: http://ogp.me/ns#">

<head>



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="Cobots (Pty) Ltd." />

	<meta name="description" content="Robotiq gives you the competitive edge you're looking for with their selection of end-effectors and accessories for colaborative robots.">

	<meta name="robots" content="index, follow">


	<?php include ("favicon.php");?>

	<?php include ("stylesheets.php");?>

	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>Brands | Robotiq | Cobots (Pty) Ltd.</title>

	<!--OPENGRAPH-->

	<meta property="og:title" content="Brands | Robotiq | Cobots (Pty) Ltd." />

	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://www.cobots.co.za/robotiq" />

	<meta property="og:image" content="https://www.cobots.co.za/assets/img/home/cobots-featured-image.png" />

		<meta property="og:image:type" content="image/png" />

		<meta property="og:image:width" content="1200" />

		<meta property="og:image:height" content="630" />

		<meta property="og:image:alt" content="Welcome to Cobots (Pty) Ltd. | Brands | Robotiq" />

</head>



<body class="stretched">


	<div id="wrapper" class="clearfix">



		<?php require("header.php"); ?>

		<script type="text/javascript" src="assets/js/lazyload.js"></script>


		<section id="brand-page-title" class="page-title-dark page-title-center" style="background-image: url('https://www.cobots.co.za/assets/img/brands/robotiq/robotiq-title-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">



			<div class="container clearfix">

				<h1 style="color:#333 !important">Introducing Robotiq</h1>

				<span style="color:#333"><strong>Giving you the competitive edge you're looking for</strong></span>

			</div>



		</section>


		<section id="content">

			

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/brands/robotiq/robotiq-intro-background.png'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<div class="col_full nobottommargin center">

						<p style="text-align: justify; color: #333333;">The team at Robotiq build their premium Plug + Play Components to be easy to program even if you don't have any robotics background. There's very little training required, and you’ll have easy access to their online community and integration coaches to help you when you need it. They’ve also created a program to help manufacturing managers build their in-house robotic knowledge, to help you take control of your robotic cell deployments.</p>

						<p style="text-align: justify; color: #333333;">Collaborative robots are easy to program by yourself. They help you achieve your targets and reduce waste on the production floor. With the Lean Robotics methodology, you can standardize on Robotiq and deploy collaborative robot cells that meet your process requirements.</p>

						</div>	

				</div>

			</div>



			<div class="content-wrap"  style="background-color:#f7f7f7">

				<div class="container clearfix">

					<div class="heading-block" style="padding-top: 40px;" data-animate="fadeInLeft">

						<h1 style="color: #333">Applications</h1>

					</div>


					<div id="portfolio" class="portfolio grid-container portfolio-6 portfolio-masonry clearfix">



						<?php include("application-desc-pick-and-place.php");?>

						

						<?php include("application-desc-bin-picking.php");?>

						

						<?php include("application-desc-surface-finishing.php");?>

						<?php include("application-desc-quality-testing.php");?>

						<?php include("application-desc-assembly.php");?>

						<?php include("application-desc-machine-tending.php");?>

					</div>
				</div>

			</div>

			<div class="content-wrap" style="background-color:#eeeeee">

				<div class="container clearfix">

							<div class="heading-block" style="padding-top: 40px;" data-animate="fadeInLeft">

							<h1 style="color: #333">Products</h1>

							</div>

							<?php include("product-card-robotiq-adaptive-grippers.php");?>

							

							

							<?php include("product-card-robotiq-3-finger-adaptive-gripper.php");?>

							

							

							<?php include("product-card-robotiq-wrist-camera.php");?>

							

							

							<?php include("product-card-robotiq-force-sensor.php");?>

							

							

							<?php include("product-card-robotiq-insights.php");?>

							

							

							<?php include("product-card-robotiq-skills.php");?>

				</div>

			</div>
			<div id="related">

				<?php include ("footer-related.php")?>
			</div>

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/brands/robotiq/robotiq-call-to-action-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<h2 style="color: #fff; text-align:center">Contact us today to add end-effectors, accessories and software from Robotiq to your automated production line.</h2>

						<div class="widget clearfix" style="text-align:center">

							<a href="https://www.cobots.co.za/contact" class="btn btn-contact">Contact Us</a>

						</div>

				</div>

			</div>

		</section>


		<?php require("footer.php"); ?>

</body>

</html>