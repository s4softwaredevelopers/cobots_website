

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/end-effectors/onrobot-dual-rg6.jpg" data-srcset="assets/img/products/end-effectors/onrobot-dual-rg6.jpg" alt="Onrobot Dual Gripper" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>Dual Gripper | OnRobot</h2>

									<p class="card-text" style="text-align:justify">Both the RG2 and the RG6 are available in a Dual Gripper configuration. This allows two grippers to be installed on the same robot arm, still without any additional cables. The two grippers work as independent grippers.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".onrobot-dual-gripper-modal">More Information</button>

										<div class="modal fade onrobot-dual-gripper-modal" tabindex="-1" role="dialog" aria-labelledby="onrobot-dual-gripper-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Dual Gripper</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>The dual configuration allows the robot arm to perform more complex tasks while notably increasing productivity, simply by being able to handle more objects at a time. It also enables the user to adapt configuration to the application, rather than requiring changes to application to accommodate automation.</p>

															<table width="100%" style="text-align:left;">

																<tbody>

																	<tr>

																		<td width="30%"><strong>No External Cables</strong></td>

																		<td width="70%">Free movement and allows full integration with UR3 infinite loop. No application time needed for cable dressing & handling.</td>

																	</tr>

																	<tr style="background-color:#f7f7f7">

																		<td width="30%"><strong>Adjustable Force and Stroke</strong></td>

																		<td width="70%"><p>Gripping force from 3N to 40N for RG2 Dual Gripper, and 25N to 120N for the RG6 Dual gripper.</p>

																		<p>Gripping stroke from 0mm to 110mm for the RG2 Dual Gripper, and 0mm to 160mm for the RG6 Dual gripper.</p></td>

																	</tr>

																	<tr>

																		<td width="30%"><strong>Grip Indications</strong></td>

																		<td width="70%">Automatic ‘Lost grip detection’, ‘grip detected’, ‘continuous grip’ and ‘measure width’ detections eliminate the need to manually program these features.</td>

																	</tr>

																	<tr style="background-color:#f7f7f7">

																		<td width="30%"><strong>Automatic Depth Compensation</strong></td>

																		<td width="70%">Gripper integrated with arm motion to assure gripper fingers sweep in parallel across work surface. Dramatically simplifies programming and avoids time extensive workarounds.</td>

																	</tr>

																	<tr>

																		<td width="30%"><strong>Automatic Payload Calculation</strong></td>

																		<td width="70%">Simplifies programming and complies with standards.</td>

																	</tr>

																	<tr style="background-color:#f7f7f7">

																		<td width="30%"><strong>Automatic Tool Center Point (TCP) Calculation</strong></td>

																		<td width="70%">Simplifies programming and avoids time extensive workarounds. Robot arm movements are more accurate.</td>

																	</tr>

																	<tr>

																		<td width="30%"><strong>Multi-Position Mounting Bracket</strong></td>

																		<td width="70%">Mount gripper in arbitrary orientation. Enables user to adapt configuration to application, rather than requiring changes to applications.</td>

																	</tr>

																	<tr style="background-color:#f7f7f7">

																		<td width="30%"><strong>Customisable Fingertips</strong></td>

																		<td width="70%">Quick finger change. Better grip and higher payload.</td>

																	</tr>

																</tbody>

															</table>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>

