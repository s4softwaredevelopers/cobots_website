

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/robots/mirhook200.png" data-srcset="assets/img/products/robots/mirhook200.png" alt="MiRHook200" align="middle" style="padding: 30px 0">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>MiRHook200 | MiR</h2>

									<p class="card-text" style="text-align:justify">Automatic pick-up and drop-off of carts up to 500 kg (1100 lbs) payload. Increase the efficiency of internal transportation tasks with an extended-payload MiRHook200™ mobile robot. MiRHook200™ is a user-friendly and efficient mobile robot for fully automated pick-up and towing of carts in production, logistics and healthcare environments.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".mir-mirhook200-modal">More Information</button>

										<div class="modal fade mir-mirhook200-modal" tabindex="-1" role="dialog" aria-labelledby="mir-mirhook200-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">MiRHook200™</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>MiRHook200™ is ideal for a wide range of towing jobs, such as efficiently moving heavy products between locations in a manufacturing facility or warehouse, or moving linen and food carts in hospitals. The MiRHook200™ supports the transport of loads up to 500 kg (1100 lbs), providing exciting new internal logistics options for weighty or unwieldy cargos.</p>

															<p>The MiRHook200™ robot identifies carts by QR markers and autonomously transports them as you define. MiRHook200™ can be incorporated into a fleet of MiR robots, and can be easily redeployed to meet changing requirements. Updating the robot's mission at any time is simple, using a smartphone, tablet or computer and standard Wi-Fi or Bluetooth communications to access the robot's intuitive controls.</p>

															<p>With the MiRHook200™, you simply measure your cart and feed the data into the software. Built-in sensors, cameras and sophisticated software mean the robot with the MiRHook200™  and a cart can safely maneuver around people and obstacles, and can even drive up ramps.</p>

															<h3>Specs</h3>

															<table width="100%" style="text-align:left;">

																<tbody>

																	<tr>

																		<td width="50%"><strong>Length</strong></td>

																		<td width="50%">1180mm to 1275 mm (highest to lowest positions of hook arm)</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Width</strong></td>

																		<td width="50%">580mm</td>

																	</tr>	

																	<tr>

																		<td width="50%"><strong>Height</strong></td>

																		<td width="50%">550mm to 900 mm (lowest to highest positions of hook arm)</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Height above floor</strong></td>

																		<td width="50%">Robot: 50 mm; Gripping height: 50-390 mm</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Weight (without load)</strong></td>

																		<td width="50%">98kg</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Load Surface</strong></td>

																		<td width="50%">1300x900</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Towing Capacity</strong></td>

																		<td width="50%">Up to 500kg (1% incline)/300kg(5% incline)</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>IP Class</strong></td>

																		<td width="50%">IP20</td>

																	</tr>

																</tbody>

															</table>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>

