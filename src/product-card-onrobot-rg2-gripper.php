

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/end-effectors/onrobot-rg2-gripper.png" data-srcset="assets/img/products/end-effectors/onrobot-rg2-gripper.png" alt="Onrobot RG2 Gripper" align="middle" style="padding:10px 0">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>RG2 Collaborative Gripper | OnRobot</h2>

									<p class="card-text" style="text-align:justify">The RG2 gripper is a collaborative End-of-Arm Tooling designed for a seamless integration with the collaborative robot arms from Universal Robots.</p>

									<p class="card-text" style="text-align:justify">The standard RG2 comes with a single mounting bracket and is also available in a precision configuration which features safety shields for the gripper fingers and a precision mounting bracket.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".onrobot-rg2-gripper-modal">More Information</button>

										<div class="modal fade onrobot-rg2-gripper-modal" tabindex="-1" role="dialog" aria-labelledby="onrobot-rg2-gripper-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">RG2 Collaborative Gripper</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>The RG2 gripper eliminates the headache by installation kits and works without any external cables, so any robot movement can be carried out without worrying about cable placement. RG2 Collaborative grippers are the true Plug & Produce solutions. The fast installation and simple programming of the grippers reduces your deployment time by 30%.</p>

															<table width="100%" style="text-align:left;">

																<tbody>

																	<tr>

																		<td width="30%"><strong>No External Cables</strong></td>

																		<td width="70%">Free movement and allows full integration with UR3 infinite loop. No application time needed for cable dressing & handling.</td>

																	</tr>

																	<tr style="background-color:#f7f7f7">

																		<td width="30%"><strong>Adjustable Force and Stroke</strong></td>

																		<td width="70%">Gripping force from 3N to 40N and gripping stroke from 0mm to 110mm.</td>

																	</tr>

																	<tr>

																		<td width="30%"><strong>Absolute Width Read Out in mm</strong></td>

																		<td width="70%">The gripper automatically detects the width at the start of the program, without initialization.</td>

																	</tr>

																	<tr style="background-color:#f7f7f7">

																		<td width="30%"><strong>Grip Indications</strong></td>

																		<td width="70%">Automatic ‘Lost grip detection’, ‘grip detected’, ‘continuous grip’ and ‘measure width’ detections eliminate the need to manually program these features.</td>

																	</tr>

																	<tr>

																		<td width="30%"><strong>Automatic Depth Compensation</strong></td>

																		<td width="70%">Gripper integrated with arm motion to assure gripper fingers sweep in parallel across work surface. Dramatically simplifies programming and avoids time extensive workarounds.</td>

																	</tr>

																	<tr style="background-color:#f7f7f7">

																		<td width="30%"><strong>Automatic Payload Calculation</strong></td>

																		<td width="70%">Simplifies programming and complies with standards.</td>

																	</tr>

																	<tr>

																		<td width="30%"><strong>Automatic Tool Center Point (TCP) Calculation</strong></td>

																		<td width="70%">Simplifies programming and avoids time extensive workarounds. Robot arm movements are more accurate.</td>

																	</tr>

																	<tr style="background-color:#f7f7f7">

																		<td width="30%"><strong>Multi-Position Mounting Bracket</strong></td>

																		<td width="70%">Mount gripper in arbitrary orientation. Enables user to adapt configuration to application, rather than requiring changes to applications.</td>

																	</tr>

																	<tr>

																		<td width="30%"><strong>Customisable Fingertips</strong></td>

																		<td width="70%">Quick finger change. Better grip and higher payload.</td>

																	</tr>

																</tbody>

															</table>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>

