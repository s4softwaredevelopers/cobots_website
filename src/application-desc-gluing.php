<article class="portfolio-item">

							<div class="portfolio-image">

									<img data-toggle="modal" data-target=".gluing-modal"  src="assets/img/icons/applications/gluing.png" alt="Gluing">

							</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".gluing-modal">Gluing</a></h3>

								<div style="text-align:center">



										<div class="modal fade gluing-modal" tabindex="-1" role="dialog" aria-labelledby="gluing-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Gluing</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>A collaborative robot can reduce waste and increase accuracy for all gluing applications. Implementing a collaborative robot will allow you to add more flexibility, efficiency and freedom to the process. The gluing application by a collaborative robot can be used in many industries as it can be used from sinks, windows to machines and automobile parts. The collaborative robot will assist in reducing cycle time and improve the quality of your products.</p> 

															<p>When gluing one has to dose exactly the same quantity of glue constantly. This is achievable with a Universal Robot as it can maintain consistent pressure at all times.  This will ensure consistency in the quality of the output, reduce production costs and optimize your operation.</p>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>

							</div>

						</article>