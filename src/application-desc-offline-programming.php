<article class="portfolio-item">

							<div class="portfolio-image">

									<img data-toggle="modal" data-target=".offline-programming-modal"  src="assets/img/icons/applications/offline-programming.png" alt="Offline Programming">

							</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".offline-programming-modal">Offline Programming</a></h3>

								<div style="text-align:center">



										<div class="modal fade offline-programming-modal" tabindex="-1" role="dialog" aria-labelledby="offline-programming-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Offline Programming</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>Off-line Programming refers to programming robots outside the production environment this eliminates production downtime which is caused by shop floor programming. </p>

															<p>Off-line programming allows for studying multiple scenarios of a work cell before setting up the production cell. This allows for common mistakes that are made when designing to be predicted in time.</p>

															<p>With RoboDK, Off-line programming has a user friendly graphical user interface to stimulate and program industrial robots. </p>

															<p>Off-line programming can also be achieved using Phython. Phyton is a programming language that lets you work faster and integrate your systems more effectively. </p>

															<p>The benefits of Off-line programming:</p>
															<div style="padding:0 100px">
															<ul>
																<li>RoboDK assists in avoiding singularities and axis limits</li>
																<li>Programming experience is not required</li>
															</ul>
															</div>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>

							</div>

						</article>