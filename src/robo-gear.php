<!DOCTYPE html>

<html dir="ltr" lang="en-US" prefix="og: http://ogp.me/ns#">

<head>



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="Cobots (Pty) Ltd." />

	<meta name="description" content="As registered distributors for Robo-Gear in South Africa, Cobots will ensure that your robot is always dressed for the occasion.">

	<meta name="robots" content="index, follow">

	<?php include ("favicon.php");?>


	<?php include ("stylesheets.php");?>

	<meta name="viewport" content="width=device-width, initial-scale=1" />

	

	<title>Brands | Robo-Gear | Cobots (Pty) Ltd.</title>

	<!--OPENGRAPH-->

	<meta property="og:title" content="Brands | Robo-Gear | Cobots (Pty) Ltd." />

	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://www.cobots.co.za/robo-gear" />

	<meta property="og:image" content="https://www.cobots.co.za/assets/img/home/cobots-featured-image.png" />

		<meta property="og:image:type" content="image/png" />

		<meta property="og:image:width" content="1200" />

		<meta property="og:image:height" content="630" />

		<meta property="og:image:alt" content="Welcome to Cobots (Pty) Ltd. | Brands | Robo-Gear" />

</head>



<body class="stretched">

	<div id="wrapper" class="clearfix">



		<?php require("header.php"); ?>

		<script type="text/javascript" src="assets/js/lazyload.js"></script>


		<section id="brand-page-title" class="page-title-dark page-title-center" style="background-image: url('https://www.cobots.co.za/assets/img/brands/robo-gear/robo-gear-title-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">



			<div class="container clearfix">

				<h1 style="color:#333 !important">Introducing Robo-Gear</h1>

				<span style="color:#333"><strong>Your one stop shop for a well-dressed robot</strong></span>

			</div>



		</section>


		<section id="content">


			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/brands/robo-gear/robo-gear-intro-background.png'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<div class="col_full nobottommargin center">

						<p style="text-align: justify; color: #333333;">Drawing from nearly 20 years of experience in the automation industry, Robo-Gear offers the largest variety of protective covers for robots, lift table bellows and skirts, cable/conduit/CAT track covers, as well as dress-pack design services (both new and rebuild). Have a need to protect something not listed, be sure to contact us as we offer custom work for your unique needs.</p>

						</div>	

				</div>

			</div>

			<div class="content-wrap" style="background-color:#eeeeee">

				<div class="container clearfix">

							<div class="heading-block" style="padding-top: 40px;" data-animate="fadeInLeft">

								<h1 style="color: #333">Products</h1>

							</div>

							<?php include("product-card-robo-gear-industrial-robot-cover.php");?>

							

							

							<?php include("product-card-robo-gear-disposable-cover.php");?>

							

							

							<?php include("product-card-robo-gear-accordion-lift-cover.php");?>

							

							

							<?php include("product-card-robo-gear-reiku-cabling-solutions.php");?>

				</div>

			</div>

			<div id="related">

				<?php include ("footer-related.php")?>
			</div>

			<div class="content-wrap" style="background-image: url('https://www.cobots.co.za/assets/img/brands/robo-gear/robo-gear-call-to-action-background.jpg'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

				<div class="container clearfix">

						<h2 style="color: #fff; text-align:center">Contact us today to add accessories from Robo-Gear to your automated production line.</h2>

						<div class="widget clearfix" style="text-align:center">

							<a href="https://www.cobots.co.za/contact" class="btn btn-contact">Contact Us</a>

						</div>

				</div>

			</div>

		</section>

		<?php require("footer.php"); ?>

</body>

</html>