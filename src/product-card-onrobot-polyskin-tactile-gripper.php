
							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/end-effectors/onrobot-polyskin-tactile-gripper.png" data-srcset="assets/img/products/end-effectors/onrobot-polyskin-tactile-gripper.png" alt="Onorobot Polyskin Tactile Gripper" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>Polyskin Tactile Gripper | OnRobot</h2>

									<p class="card-text" style="text-align:justify">Cost-effective material handling with built-in force control.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".onrobot-polyskin-tactile-gripper-modal">More Information</button>

										<div class="modal fade onrobot-polyskin-tactile-gripper-modal" tabindex="-1" role="dialog" aria-labelledby="onrobot-polyskin-tactile-gripper-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Polyskin Tactile Gripper</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body modal-list">

															<p>OnRobot Polyskin Tactile grippers can provide accurate position-orientation and force data for all of your automation needs. It has ability to assess grip quality and prevent dropping items in a cost-effective way.</p>

															<p><strong>Features</strong></p>
	
																<ul>
																	<li>Individually actuated fingers</li>

																	<li>Compliant tactile sensor on fingers</li>

																	<li>Force or position controlled gripping</li>

																	<li>0-70mm stroke and up to 400N gripping force</li>

																	<li>Bump detection</li>

																	<li>Thermal sensor</li>
																</ul>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>
