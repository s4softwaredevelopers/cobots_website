

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/accessories/robo-gear-accordion-lift-cover.png" data-srcset="assets/img/products/accessories/robo-gear-accordion-lift-cover.png" alt="Robo-Gear Lift Cover" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>Custom Cable/Conduit Covers | Robo-Gear</h2>

									<p class="card-text" style="text-align:justify">Robo-Gear offers a wide variety of fire-resistant aluminized cloth (preventing spark damage to cable and hoses), CAT track covers, lift table skirts and bellows custom designed to your exact specification. Insure your robot's longevity by investing in one of these premium covers.</p>

								</div>



							</div>

