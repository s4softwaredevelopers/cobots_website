

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/robots/ur5e.png" data-srcset="assets/img/products/robots/ur5e.png" alt="UR5e" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>UR5e | Universal Robots</h2>

									<p class="card-text" style="text-align:justify">The latest offering in our line of collaborative robots, the UR5e empower future-ready change agents with collaborative innovations, a Human Centric UX, and an ecosystem for every application. Transform ambitions into results by changing the way things are made with the most flexible automation platform</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".universal-robot-ur5e-modal">More Information</button>

										<div class="modal fade universal-robot-ur5e-modal" tabindex="-1" role="dialog" aria-labelledby="universal-robot-ur5e-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">UR5e</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<table width="100%">
																<tbody>
																	<tr><td><strong>Performance</strong></td></tr>
																	<tr>
																		<td width="30%">Power Consumption</td>
																		<td width="70%">Approx. 200 W using a typical program</td>
																	</tr>
																	<tr>
																		<td width="30%">Collaboration Operation</td>
																		<td width="70%">All 17 advanced adjustable safety functions incl. elbow monitoring certified to Cat.3, PL d. Remote Control according to ISO 10218</td>
																	</tr>
																	<tr><td><strong>Specification</strong></td></tr>
																	<tr>
																		<td width="30%">Payload</td>
																		<td width="70%">5 kg / 11 lbs</td>
																	</tr>
																	<tr>
																		<td width="30%">Reach</td>
																		<td width="70%">850 mm / 33.5 in</td>
																	</tr>
																	<tr>
																		<td width="30%">Degrees of freedom</td>
																		<td width="70%">6 rotating joints DOF</td>
																	</tr>
																	<tr>
																		<td width="30%">Programming</td>
																		<td width="70%">Polyscope graphical user interface on 12 inch touchscreen with mounting</td>
																	</tr>
																	
																</tbody>
															</table>
															<span class="btn btn-primary"><a href="https://www.cobots.co.za/docs/ur5e-technical-specifications.pdf" target="blank" style="color:#ffffff !important">View UR5e technical specifications</a></span>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>
