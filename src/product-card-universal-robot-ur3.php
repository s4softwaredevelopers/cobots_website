

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/robots/ur3.png" data-srcset="assets/img/products/robots/ur3.png" alt="UR3" align="middle">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>UR3 | Universal Robots</h2>

									<p class="card-text" style="text-align:justify">The UR3 collaborative robot is a smaller collaborative table-top robot, perfect for light assembly tasks and automated workbench scenarios. The compact table-top cobot weighs only 24.3 lbs (11 kg), but has a payload of 6.6 lbs (3 kg), &#177;360-degree rotation on all wrist joints, and infinite rotation on the end joint.</p>
										<button class="btn btn-primary" data-toggle="modal" data-target=".universal-robot-ur3-modal">More Information</button>

										<div class="modal fade universal-robot-ur3-modal" tabindex="-1" role="dialog" aria-labelledby="universal-robot-ur3-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">UR3</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<table width="100%">
																<tbody>
																	<tr><td><strong>Performance</strong></td></tr>
																	<tr>
																		<td width="30%">Power Consumption</td>
																		<td width="70%">Min 90W, Typical 125W, Max 250W</td>
																	</tr>
																	<tr>
																		<td width="30%">Collaboration Operation</td>
																		<td width="70%">15 advanced adjustable safety functions. T&#252;V NORD Approved Safety Function. Tested in accordance with: EN ISO 13849:2008 PL d</td>
																	</tr>
																	<tr><td><strong>Specification</strong></td></tr>
																	<tr>
																		<td width="30%">Payload</td>
																		<td width="70%">3 kg/ 6.6 lbs</td>
																	</tr>
																	<tr>
																		<td width="30%">Reach</td>
																		<td width="70%">500mm/ 19.7 in</td>
																	</tr>
																	<tr>
																		<td width="30%">Degrees of freedom</td>
																		<td width="70%">6 rotating joints</td>
																	</tr>
																	<tr>
																		<td width="30%">Programming</td>
																		<td width="70%">Polyscope graphical user interface on 12 inch touchscreen with mounting</td>
																	</tr>
																	
																</tbody>
															</table>
															<span class="btn btn-primary"><a href="https://www.cobots.co.za/docs/ur3-technical-specifications.pdf" target="blank" style="color:#ffffff !important">View UR3 technical specifications</a></span>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>
