<article class="portfolio-item">
	<div class="portfolio-image">
			<img src="https://www.cobots.co.za/assets/img/brands/robotiq/robotiq-brand-card.jpg" alt="Robotiq">
		<div class="portfolio-overlay"></div>
	</div>
	<div class="portfolio-desc" style="background-color:#fff">
		<h3><a href="https://www.cobots.co.za/robotiq">Robotiq</a></h3>
		<p>End-Effectors | Accessories | Software</p>
			<a class="btn btn-primary" href="https://www.cobots.co.za/robotiq">View Page</a>
	</div>
</article>