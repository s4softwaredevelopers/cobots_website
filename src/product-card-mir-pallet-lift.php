

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/robots/mir-pallet-lift.png" data-srcset="assets/img/products/robots/mir-pallet-lift.png" alt="MiR500 Pallet Lift" align="middle" style="padding: 15px 0"">

									</div>

								</div>



								<div class="pricing-action-area product-card">

									<h2>MiR500 Pallet Lift | MiR</h2>

									<p class="card-text" style="text-align:justify">MiR500™  Pallet Lift enables the MiR500™  to autonomously pick up and drop off pallets and ensures stable handling and transport of your pallets. It enables the MiR500™  to lift pallets autonomously from the MiR500™  Pallet Rack and lowers the pallet for stable transportation of payloads up to 500 kg.</p>

										<button class="btn btn-primary" data-toggle="modal" data-target=".mir-pallet-lift-modal">More Information</button>

										<div class="modal fade mir-pallet-lift-modal" tabindex="-1" role="dialog" aria-labelledby="mir-pallet-lift-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">MiR500™  Pallet Lift</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>With the MiR500™  Pallet Lift, the MiR500™  picks up, transports and unloads pallets autonomously, freeing up employees for more valuable tasks. MiR500™  has a speed of 2 m/sec (7,2 km/hour) for extremely efficient transportation time. There is no need to change the facility layout when operating the MiR500™  as the function of wires, magnets or QR codes is replaced by sophisticated navigation software. Simply download CAD files of the facility to the robot or use its laser scanners to create a map - that's all it takes to ensure a fast and cost-efficient implementation.</p>

															<h3>Specs</h3>

															<table width="100%" style="text-align:left;">

																<tbody>

																	<tr>

																		<td width="50%"><strong>Length</strong></td>

																		<td width="50%">1200mm</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Width</strong></td>

																		<td width="50%">162mm</td>

																	</tr>	

																	<tr>

																		<td width="50%"><strong>Height when lowered</strong></td>

																		<td width="50%">95mm</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Total height when lifted</strong></td>

																		<td width="50%">155mm</td>

																	</tr>

																	<tr>

																		<td width="50%"><strong>Pallet Lift Payload</strong></td>

																		<td width="50%">500kg</td>

																	</tr>

																</tbody>

															</table>

														</div>

													</div>

												</div>

											</div>

										</div>

								</div>



							</div>

