<article class="portfolio-item">

							<div class="portfolio-image">

									<img data-toggle="modal" data-target=".machine-tending-modal"  src="assets/img/icons/applications/machine-tending.png" alt="Machine Tending">

							</div>

							<div class="portfolio-desc">

								<h3><a data-toggle="modal" data-target=".machine-tending-modal">Machine Tending</a></h3>

								<div style="text-align:center">



										<div class="modal fade machine-tending-modal" tabindex="-1" role="dialog" aria-labelledby="machine-tending-modal" aria-hidden="true">

											<div class="modal-dialog modal-lg">

												<div class="modal-body">

													<div class="modal-content">

														<div class="modal-header">

															<h2 class="modal-title">Machine Tending</h2>

															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

														</div>

														<div class="modal-body">

															<p>A collaborative robot can be used for many machine tending applications. By automating your machine tending job, one does not only increase safety for your workers, one also increases productivity and allows for production to continue after operators have left the workshop.</p> 

															<p>Improve the speed and the process quality while reducing the list of injury with working in close proximity to heavy machinery.</p> 

															<p>OnRobot accessories such as their dual gripper solution can significantly decrease cycle times as it is able to handle two objects at the same time. Accessories from OnRobot that are ideal for this application are:</p>
															<div style="padding: 0 100px">
															<ul>
																<li>Dual Gripper</li>
																<li>RG6 Gripper</li>
																<li>RG2 Gripper</li>
															</ul> 
															</div>
														</div>

													</div>

												</div>

											</div>

										</div>

								</div>

							</div>

						</article>