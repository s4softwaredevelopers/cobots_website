

							<div class="pricing-box pricing-extended bottommargin clearfix">



								<div class="pricing-desc">

									<div class="pricing-features">

										<img class="lazy" src="/assets/img/home/lazy-placeholder.png" data-src="assets/img/products/accessories/qbrobotics-c-flange.png" data-srcset="assets/img/products/accessories/qbrobotics-c-flange.png" alt="qbRobotics C Flange" align="middle">

									</div>

								</div>



								<div class="pricing-action-area  product-card">

									<h2>C Flange | qbRobotics</h2>

									<p class="card-text" style="text-align:justify">With this flange you can connect two qbmoves to obtain a revolute joint.</p>

								</div>

							</div>